<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define( 'JETPACK_DEV_DEBUG', true);

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'extranet');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


ini_set('log_errors','On');
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', false);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7^bd@D%QvQ{ pY y^82ujN 3n7w{eNQ:ZdkEPIP](>cf+ONiNHx8Rz=L#Gj/N7mo');
define('SECURE_AUTH_KEY',  '*5>>zAOMi^zi{M[i0q/-%2_h0:w1n516]L>;4m6+B,X#dv1i->V>{$M9qm8NL^>s');
define('LOGGED_IN_KEY',    'S@0ZHtF^VfL:-9[|p3PE+(0IS5VMN[A?!)j2 5gLgk|@o85.Oc8B(k1TanSa#9N.');
define('NONCE_KEY',        '7R1&0OMgZPXOtv;KZ>qZlZU@>]jPc CQ6hVyT[9C}r9Uy*@Jr8F@DW)r*`sH!hGc');
define('AUTH_SALT',        '2BrtNY6LOAeJMWttzIheRj6=es$5Bz_yxLVkhUDns51&7QMd*!QY<e60Wq//m9^P');
define('SECURE_AUTH_SALT', '%LOIuW+lVqj$mEP<.#%]Bdv8Xg8plPhFC)++V^;Br!<sfl p,RV;m7+F*%GgYT3C');
define('LOGGED_IN_SALT',   '%80*e5j(dDbwCZn*w*ARU#;ipuTK<4eg8-FfcBT_==#_.I[~!Dxd&7+IH6U;>g6W');
define('NONCE_SALT',       'Pdt:|LW)u}kt`hurHuxT`_nz[GCF$|(aV&#U2],2H,7O*B}]o8/6f.dec-}gf$)<');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'extrn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

// Add theme support for Featured Images
add_theme_support('post-thumbnails', array(
'post',
'page',
'gallery',
));

