<?php
/* This file contains the form for Edit Event with dropdown selection for Location */

if( !@$EM_Location ) {
	$EM_Location = $EM_Event->get_location();
}
$value 		= get_option('stonehenge-em-osm');
$zoom 		= !empty($value['zoom']) ? $value['zoom'] : '15';
$setZoom 	= $zoom;
$default 	= "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
$mapType 	= !empty($value['type']) ? $value['type'] : $default;
$icon		= !empty($value['marker']) ? $value['marker'] : 'blue';
$marker_url	= plugins_url('../inc/markers/marker-icon-2x-'. $icon . '.png', __FILE__);
$shadow_url = plugins_url('../inc/markers/marker-shadow.png', __FILE__);
$latitude	= $EM_Location->location_latitude;
$longitude 	= $EM_Location->location_longitude;
if( !$latitude || $latitude === '0') {
	$zoom = '1';
}

?>
<div id="em-location-data" class="em-location-data">
	<div id="location_coordinates" style='display: none;'>
		<input id='location-latitude' name='location_latitude' type='text' value='<?php echo esc_attr($EM_Event->get_location()->location_latitude); ?>' size='15' />
		<input id='location-longitude' name='location_longitude' type='text' value='<?php echo esc_attr($EM_Event->get_location()->location_longitude); ?>' size='15' />
	</div>

	<table class="em-location-data">
		<tr class="em-location-data-select">
			<th><?php esc_html_e('Location:','events-manager') ?> </th>
			<td>
				<select name="location_id" id='location-select-id' size="1" onChange="selectCoords()">
					<?php if(!get_option('dbem_require_location',true)): ?><option value="0" title="0,0"><?php esc_html_e('No Location','events-manager'); ?></option><?php endif; ?>
					<?php
					$ddm_args = array('private'=>$EM_Event->can_manage('read_private_locations'));
					$ddm_args['owner'] = (is_user_logged_in() && !current_user_can('read_others_locations')) ? get_current_user_id() : false;
					$locations = EM_Locations::get($ddm_args);
					$selected_location = !empty($EM_Event->location_id) || !empty($EM_Event->event_id) ? $EM_Event->location_id:get_option('dbem_default_location');
					foreach($locations as $EM_Location) {
						$selected = ($selected_location == $EM_Location->location_id) ? "selected='selected' " : '';
						if( $selected ) $found_location = true;
				   		?>
				    	<option value="<?php echo esc_attr($EM_Location->location_id) ?>" title="<?php echo esc_attr("{$EM_Location->location_latitude},{$EM_Location->location_longitude}"); ?>" <?php echo $selected ?>><?php echo esc_html($EM_Location->location_name); ?></option>
				    	<?php
					}
					if( empty($found_location) && !empty($EM_Event->location_id) ){
						$EM_Location = $EM_Event->get_location();
						if( $EM_Location->post_id ){
							?>
					    	<option value="<?php echo esc_attr($EM_Location->location_id) ?>" title="<?php echo esc_attr("{$EM_Location->location_latitude},{$EM_Location->location_longitude}"); ?>" selected="selected"><?php echo esc_html($EM_Location->location_name); ?></option>
					    	<?php
						}
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<th><?php echo __('Map:', 'stonehenge-em-osm'); ?></th>
			<td>
				<script>
				var Lat = jQuery("#location-latitude").val();
				var Lng = jQuery("#location-longitude").val();
				var balloon = jQuery("#location-select-id option:selected").text();
				const zoomLevel = <?php echo $zoom;?>;
				const setZoom = <?php echo $setZoom; ?>;
				const myTiles = '<?php echo $mapType; ?>';
				const IconUrl = '<?php echo $marker_url; ?>';
				const IconShadow = '<?php echo $shadow_url; ?>';

				function selectCoords() {
					var Coords = jQuery("#location-select-id option:selected").attr('title').split(",");
					var newLat = Coords[0];
					var newLng = Coords[1];

					jQuery("#location-latitude").val(newLat);
					jQuery("#location-longitude").val(newLng);

					var newLatLng = new L.LatLng(newLat, newLng);
					var newBalloon = jQuery("#location-select-id option:selected").text();
					marker.setLatLng(newLatLng).bindPopup(newBalloon);
					map.setView(newLatLng, setZoom);
				}
				</script>
				<div id="em-osm-map-container">
					<div id="em-osm-map" style="width: 400px; height: 300px;"></div>
					<?php echo self::load_map_files(); ?>
				</div>
			</td>
		</tr>
