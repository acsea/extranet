<?php
/************************************************************
* Plugin Name:	Events Manager – OpenStreetMaps
* Plugin URI: 	https://wordpress.org/plugins/stonehenge-em-osm/
* Description:	OpenStreetMaps replacement for Events Manager.
* Version:		1.8.5.1
* Author:  		Stonehenge Creations
* Author URI: 	https://www.stonehengecreations.nl/
* License URI: 	https://www.gnu.org/licenses/gpl-2.0.html
* Text Domain: 	stonehenge-em-osm
* Domain Path: 	/languages
************************************************************/

// Exit if accessed directly
if (!defined('ABSPATH')) exit;

include_once(ABSPATH.'wp-admin/includes/plugin.php');


#===============================================
# Check for Events Manager.
#===============================================
if( is_plugin_active('events-manager/events-manager.php') ) {
	add_action('plugins_loaded', 'start_stonehenge_em_osm');
}
else {
	deactivate_plugins(plugin_basename(__FILE__));
	wp_die('<b>EM OpenStreetMaps</b> needs Events Manager to be installed and activated.<br>This plugin has been deactivated...', 'Plugin dependency check', array('back_link' => true));
}


function start_stonehenge_em_osm() {
	$stonehenge_em_osm = new Stonehenge_EM_OSM;
}


class Stonehenge_EM_OSM {

	private $version = '1.8.5';

	public function __construct() {

		// Common Plugin Actions.
		add_action('init', array($this, 'load_textdomain'));
		add_filter('plugin_action_links', array($this, 'add_settings_link'), 10, 2);
		add_filter('plugin_row_meta', array($this, 'create_additional_plugin_links'), 10, 2);
		register_deactivation_hook(__FILE__, array( $this, 'remove_template_files'));

		// Admin Functions.
		add_action('admin_menu', array($this, 'add_submenu'), 49, 2);
		add_action('admin_init', array($this, 'register_settings'));
		add_action('admin_notices', array($this, 'create_admin_notices') );
		add_action('wp_loaded', array($this, 'rename_template_event'), 11, 1);
		add_action('wp_loaded', array($this, 'rename_template_location'), 12, 1);

		// Front-End Functions.
		add_action('wp_loaded', array($this, 'disable_google_api'), 50);
		add_action('wp_head', array($this, 'preload_openstreetmaps'), 1);

		include('osm-shortcodes.php');
		$stonehenge_em_osm_shortcodes = new Stonehenge_EM_OSM_Shortcodes;
	}


	#===============================================
	#  	Make this Plugin translatable.
	#===============================================
	function load_textdomain() {
		$wplang = !empty(get_option("WPLANG")) ? get_option("WPLANG") : 'en_US';
		$locale = str_replace("_formal", "@formal", $wplang);
		$slug 	= 'stonehenge-em-osm';
		$langs 	= WP_CONTENT_DIR . '/languages/plugins/';
		$plugin = plugin_dir_path( __FILE__ ) . 'languages/';
		$po 	= '.po';
		$mo 	= '.mo';
		// .po files
		if( file_exists($plugin.$locale.$po) && !file_exists($langs.$slug.'-'.$locale.$po) ) {
			copy($plugin.$locale.$po, $langs.$slug.'-'.$locale.$po);
		}
		// .mo files
		if( file_exists($plugin.$locale.$mo) && !file_exists($langs.$slug.'-'.$locale.$mo) ) {
			copy($plugin.$locale.$mo, $langs.$slug.'-'.$locale.$mo);
		}
	    load_plugin_textdomain($slug, FALSE, basename( dirname( __FILE__ ) ) . '/languages/');
	}


	#===============================================
	# 	Add "Settings" link to WP Plugins Page.
	#===============================================
	function add_settings_link( $links, $file ) {
		if ($file != plugin_basename(__FILE__)) {
			return $links;
		}
		else {
			$settings_link = '<a href="'.admin_url('edit.php?post_type='.EM_POST_TYPE_EVENT.'&page='.'stonehenge-em-osm').'">'. __('Settings').'</a>';
			array_unshift($links, $settings_link);
			return $links;
		}
	}


	#===============================================
	# 	Add additional links to WP Plugins Page.
	#===============================================
	function create_additional_plugin_links($links, $file) {
		$plugin = plugin_basename(__FILE__);
		if ($file == $plugin) { // Only for this plugin
			$plugindir  = explode("/plugins/", dirname(__FILE__));
			$wpslug		  = $plugindir[1];
			$author		  = 'DuisterDenHaag';
			$donate		  = 'https://useplink.com/payment/VRR7Ty32FJ5mSJe8nFSx';
			return array_merge(
				$links,
				array('<a href="https://wordpress.org/support/plugin/'.$wpslug.'" target="_blank">'.__('Plug-in Support', 'stonehenge-em-osm').'</a>'),
				array('<a href="https://profiles.wordpress.org/'.$author.'" target="_blank">'.__('WordPress Profile', 'stonehenge-em-osm').'</a>'),
				array('<a href="'.$donate.'" target="_blank">'.__('Donate', 'stonehenge-em-osm').'</a>')
			);
		}
		return $links;
	}


	#===============================================
	# 	Remove template files upon deactivation.
	#===============================================
	public static function remove_template_files() {
		$theme_location = get_stylesheet_directory().'/plugins/events-manager/forms/event/location.php';
		$theme_event 	= get_stylesheet_directory().'/plugins/events-manager/forms/location/where.php';

		if( file_exists($theme_location) ) {
			wp_delete_file($theme_location);
		}
		if( file_exists($theme_event) ) {
			wp_delete_file($theme_event);
		}
	}


	#===============================================
	# 	Preload OSM Map Tiles.
	#===============================================
	function preload_openstreetmaps() {
		echo '<link rel="preconnect" crossorigin="" href="//tile.openstreetmap.org">';
	}


	#===============================================
	# 	Show Admin Notice if OpenCage API is missing.
	#===============================================
	function create_admin_notices() {
		$settingsUrl 	= admin_url('edit.php?post_type=event&page=stonehenge-em-osm');
		$message 		= '';
		$options 		= get_option('stonehenge-em-osm');

		if ( !$options['api'] ) {
			$message .= '<div class="notice notice-error"><p>';
			$message .= sprintf( __('Please enter your OpenCage API Key in your <a href="%s">Plugin Settings</a>.', 'stonehenge-em-osm'),
				$settingsUrl
			);
			$message .= '</p></div>';
		}
		echo $message;
	}


	#===============================================
	# 	Make sure the right templates are used.
	#===============================================
	public static function rename_template_event() {
		$version 		= '1.8.5';
		$file 			= 'where.php';
		$theme_folder 	= get_stylesheet_directory() . '/plugins/events-manager/forms/location/';
		$plugin_folder	= plugin_dir_path(__FILE__) . 'templates/';
		$plugin_file 	= $plugin_folder.$file;
		$theme_file 	= $theme_folder.$file;
		$version_file 	= $theme_folder.'where-'.$version.'.php';

		// Clean install.
		if( !is_dir($theme_folder) && !file_exists($theme_file) && !file_exists($version_file) ) {
			wp_mkdir_p($theme_folder);
			copy($plugin_file, $theme_file);
			rename($theme_file, $version_file);
			copy($plugin_file, $theme_file);
		}

		// If folder exists, but the files not.
		elseif( is_dir($theme_folder) && !file_exists($theme_file) && !file_exists($version_file) ) {
			copy($plugin_file, $theme_file);
			rename($theme_file, $version_file);
			copy($plugin_file, $theme_file);
		}

		// If folder and template exist, back-up old template.
		elseif( is_dir($theme_folder) && file_exists($theme_file) && !file_exists($version_file) ) {
			rename($theme_file, $version_file);
			copy($plugin_file, $theme_file);
		}

		// If folder and back-up exist, but main template is missing.
		elseif( is_dir($theme_folder) && !file_exists($theme_file) && file_exists($version_file) ) {
			copy($plugin_file, $theme_file);
		}

		else {
			return;
		}
	}

	public static function rename_template_location() {
		$version 		= '1.8.5';
		$file 			= 'location.php';
		$theme_folder 	= get_stylesheet_directory() . '/plugins/events-manager/forms/event/';
		$plugin_folder	= plugin_dir_path(__FILE__) . 'templates/';
		$plugin_file 	= $plugin_folder.$file;
		$theme_file 	= $theme_folder.$file;
		$version_file 	= $theme_folder.'location-'.$version.'.php';

		// Clean install.
		if( !is_dir($theme_folder) && !file_exists($theme_file) && !file_exists($version_file) ) {
			wp_mkdir_p($theme_folder);
			copy($plugin_file, $theme_file);
			rename($theme_file, $version_file);
			copy($plugin_file, $theme_file);
		}

		// If folder exists, but the files not.
		elseif( is_dir($theme_folder) && !file_exists($theme_file) && !file_exists($version_file) ) {
			copy($plugin_file, $theme_file);
			rename($theme_file, $version_file);
			copy($plugin_file, $theme_file);
		}

		// If folder and template exist, back-up old template.
		elseif( is_dir($theme_folder) && file_exists($theme_file) && !file_exists($version_file) ) {
			rename($theme_file, $version_file);
			copy($plugin_file, $theme_file);
		}

		// If folder and back-up exist, but main template is missing.
		elseif( is_dir($theme_folder) && !file_exists($theme_file) && file_exists($version_file) ) {
			copy($plugin_file, $theme_file);
		}

		else {
			return;
		}
	}


	#===============================================
	# 	Make sure Google Maps integration is deactivated.
	#===============================================
	function disable_google_api() {
		if( '0' != get_option('dbem_gmap_is_active') ) {
			update_option( 'dbem_gmap_is_active', '0' );
		}
	}


	#===============================================
	# 	Show the Location MetaBox in Edit Location Pages.
	#===============================================
	public static function show_osm_meta_box() {
		global $EM_Location, $EM_Event, $post;
		$required 	= apply_filters('em_required_html','<i>*</i>');
		?>
		<input type="hidden" name="_emnonce" value="<?php echo wp_create_nonce('edit_location'); ?>">
		<div id="em-location-data" class="em-location-data">
			<table class="em-location-data">
				<?php include('partials/location-form-fields.php'); ?>
			<br style="clear:both;">
		</div>
		<?php
	}


	// Workaround for templates not being replaced.
	public static function construct_admin_map() {
		global $EM_Event;
		return self::show_edit_event_box( $EM_Event );
	}


	#===============================================
	# 	Show the Location MetaBox in Edit Event Pages.
	#===============================================
	public static function show_edit_event_box( $EM_Event ) {
		global $EM_Event;
		$required = apply_filters('em_required_html','<i>*</i>');

		//------------------------------------------------------
		// 	If "No Location" is checked.
		//------------------------------------------------------
		if( !get_option('dbem_require_location') && !get_option('dbem_use_select_for_locations') ) {
			?><div class="em-location-data-nolocation">
				<p>
					<input type="checkbox" name="no_location" id="no-location" value="1" <?php if( $EM_Event->location_id === '0' || $EM_Event->location_id === 0 ) echo 'checked="checked"'; ?> />
					<?php _e('This event does not have a physical location.','events-manager'); ?>
				</p>
				<script type="text/javascript">
					jQuery(document).ready(function($){
						$('#no-location').change(function(){
							if( $('#no-location').is(':checked') ){
								$('#em-location-data').hide();
							}else{
								$('#em-location-data').show();
							}
						}).trigger('change');

					});
				</script>
			</div>
			<?php
		}

		// If a location is required, create a div for location details.
		?>
		<div id="em-location-data" class="em-location-data">
			<table class="em-location-data">
				<?php
				//------------------------------------------------------
				// 	If "Select Dropdown" is used.
				//------------------------------------------------------
				if( get_option('dbem_use_select_for_locations') || !$EM_Event->can_manage('edit_locations','edit_others_locations') ) {
						include('partials/location-select.php');
				}

				//------------------------------------------------------
				// 	If "Location Form Fields" are used.
				//------------------------------------------------------
				else {
					if( empty($EM_Event->location_id) ) {
						if(get_option('dbem_default_location') > 0){
							$EM_Location = em_get_location(get_option('dbem_default_location'));
						}
						else {
							$EM_Location = new EM_Location();
						}
					}
					else {
						$EM_Location = $EM_Event->get_location();
					}
				?>
				<tr class="em-location-data-name">
					<th><?php _e ( 'Location Name:', 'events-manager')?>&nbsp;</th>
					<td>
						<input id="location-id" name="location_id" type="hidden" value="<?php echo esc_attr($EM_Location->location_id); ?>">
						<input id="location-name" type="text" name="location_name" value="<?php echo esc_attr($EM_Location->location_name, ENT_QUOTES); ?>" style="width:250px !important;" onFocusOut="searchCoords()"> <?php echo $required; ?>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<em id="em-location-search-tip"><?php /*esc_html_e('Create a location by using the Search Box on the map or start typing to search a previously created location.', 'stonehenge-em-osm')*/?></em>
						<em id="em-location-reset" style="display:none;"><?php esc_html_e('You cannot edit saved locations here.', 'events-manager'); ?><br><a href="#"><?php esc_html_e('Reset this form to create a location or search again.', 'events-manager')?></a></em>
					</td>
					</tr>
					<!-- Show/hide the Search Address button -->
					<script type="text/javascript">
					jQuery(document).ready(function($){
						if( jQuery('#location-name').is('[readonly]') ) {
							jQuery('#osm-button').css("display", "none");
						}

						$('#location-name').change(function(){
							if( jQuery('#location-name').is('[readonly]') ) {
								jQuery('#osm-button').css("display", "none");
							}
							else {
								jQuery('#osm-button').css("display", "");
							}
						});
					}).trigger('change');
					jQuery('#em-location-reset a').click( function(){
						$('#osm-button').show();
					});
					</script>
				<?php
					include('partials/location-form-fields.php'); // This also closes the <table> before showing the map.
				}
			?>
			</table>
			<br style="clear:both;" />
		</div>
		<?php
	}


	#===============================================
	# 	Combine Single Location Files in one function.
	#===============================================
	public static function load_map_files() {
		wp_enqueue_style( 'em-osm-css', plugins_url('inc/em-osm.min.css', __FILE__));
		wp_enqueue_script( 'em-osm-core', plugins_url('inc/em-osm-core.min.js', __FILE__), array(), '', true);
		wp_enqueue_script( 'em-osm-single', plugins_url('inc/em-osm-single.min.js', __FILE__), array('em-osm-core'), '', true);
	}


	#===============================================
	# 	Show the map when editing or adding a location
	#===============================================
	public static function show_edit_location_map( $EM_Location ) {
		$locale 	= strtoupper(substr( get_bloginfo ( 'language' ), 0, 2 )); // Get the first 2 letters of the current language
		$value 		= get_option('stonehenge-em-osm');
		$zoom 		= !empty($value['zoom']) ? $value['zoom'] : '15';
		$setZoom 	= $zoom;
		$api 		= !empty($value['api']) ? $value['api'] : '';
		$default 	= "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
		$mapType 	= !empty($value['type']) ? $value['type'] : $default;
		$icon		= !empty($value['marker']) ? $value['marker'] : 'blue';
		$marker_url	= plugins_url('inc/markers/marker-icon-2x-'. $icon . '.png', __FILE__);
		$shadow_url = plugins_url('inc/markers/marker-shadow.png', __FILE__);
		$latitude	= $EM_Location->location_latitude;
		$longitude 	= $EM_Location->location_longitude;
		if( !$latitude || $latitude === '0') {
			$zoom = '1';
		}
		$balloon 	= ($EM_Location->location_name) ? $EM_Location->location_name : "";

		?>
		<script>
			const api = '<?php echo $api; ?>';
			const locale = '<?php echo $locale; ?>';
			const balloon = '<?php echo $balloon; ?>';
			const zoomLevel = <?php echo $zoom;?>;
			const setZoom = <?php echo $setZoom; ?>;
			const myTiles = '<?php echo $mapType; ?>';
			let Lat = <?php echo $latitude; ?>;
			let Lng = <?php echo $longitude; ?>;
			const IconUrl = '<?php echo $marker_url; ?>';
			const IconShadow = '<?php echo $shadow_url; ?>';
		</script>
		<div id="em-osm-map-container">
			<div id="em-osm-map" style="width: 400px; height: 300px;"></div>
			<?php echo Stonehenge_EM_OSM::load_map_files(); ?>
		</div>
		<?php
	}


	#===============================================
	# 	Create Single Location Map
	#===============================================
	public static function construct_single_location_map($EM_Location) {
		global $EM_Event, $EM_Location;

		// Check if we are in the EM_Object.
		if(!$EM_Event && !$EM_Location) {
			return;
		}

		// If this is an Event Object, create new Location Object.
		if(!$EM_Location) {
			$EM_Location = new EM_Location($EM_Event->location_id);
		}

		$balloon 	= trim(preg_replace('/\s\s+/', '<br>', get_option('dbem_location_baloon_format')));
		if( $EM_Event ) {
			$balloon 	= $EM_Event->output($balloon);
		}
		$balloon 	= $EM_Location->output($balloon);

		$balloon 	= addslashes($balloon);
		$longitude	= $EM_Location->location_longitude;
		$latitude	= $EM_Location->location_latitude;
		$value 		= get_option('stonehenge-em-osm');
		$zoom 		= !empty($value['zoom']) ? $value['zoom'] : '15';
		$default 	= "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
		$mapType 	= !empty($value['type']) ? $value['type'] : $default;
		$icon		= !empty($value['marker']) ? $value['marker'] : 'blue';
		$marker_url	= plugins_url('inc/markers/marker-icon-2x-'. $icon . '.png', __FILE__);
		$shadow_url = plugins_url('inc/markers/marker-shadow.png', __FILE__);
		$width 		= get_option('dbem_map_default_width') ? get_option('dbem_map_default_width') : '400px';
		$width 		= preg_match('/(px)|%/', $width) ? $width : $width.'px';
		$height 	= get_option('dbem_map_default_height') ? get_option('dbem_map_default_height') : '300px';
		$height 	= preg_match('/(px)|%/', $height) ? $height:$height.'px';

		// Show a warning if coordinates are missing.
		if(!$latitude || $latitude === '0.000000') {
			$output = '<p class="em-warning em-warning-errors">'. __('Location coordinates could not be found.', 'stonehenge-em-osm') .'</p>';
		}
		// Start output.
		else {
			ob_start();
			?>
			<script>
				let zoomLevel = <?php echo $zoom; ?>;
				const Lat = <?php echo $latitude; ?>;
				const Lng = <?php echo $longitude; ?>;
				const balloon = '<?php echo $balloon; ?>';
				const myTiles = '<?php echo $mapType; ?>';
				const IconUrl = '<?php echo $marker_url; ?>';
				const IconShadow = '<?php echo $shadow_url; ?>';
			</script>
			<div id="em-osm-map-container">
				<div id="em-osm-map" style="width: <?php echo $width; ?>; height: <?php echo $height; ?>;"></div>
				<?php echo Stonehenge_EM_OSM::load_map_files(); ?>
			</div>
			<?php
			$output = ob_get_clean();
		}
		return $output;
	}


	#===============================================
	# 	Create All Locations Map for [locations_map]
	#===============================================
	public static function construct_multiple_map( $args ) {
		global $EM_Event, $EM_Location, $EM_Object;

		// Create variables for the js.
		$value 		= get_option('stonehenge-em-osm');
		$zoom 		= !empty($value['zoom']) ? $value['zoom'] : '15';
		$default 	= "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
		$mapType 	= !empty($value['type']) ? $value['type'] : $default;
		$icon		= !empty($value['marker']) ? $value['marker'] : 'blue';
		$marker_url	= plugins_url('inc/markers/marker-icon-2x-'. $icon . '.png', __FILE__);
		$shadow_url = plugins_url('inc/markers/marker-shadow.png', __FILE__);

		// Create fallback to prevent errors.
		if(empty($args)) {
			$args = array();
		}
		$width 		= @$args['width'] ? @$args['width'] : get_option('dbem_map_default_width');
		$width 		= preg_match('/(px)|%/', $width) ? $width : $width.'px';
		$height 	= @$args['height'] ? @$args['height'] : get_option('dbem_map_default_height');
		$height 	= preg_match('/(px)|%/', $height) ? $height : $height.'px';
		$return 	= '';
		$check 		= EM_Locations::get( $args, $count=true );
		if( $check === '0' ) {
			echo '<p><em>'. __('No locations found', 'stonehenge-em-osm').'</em></p>';
			return;
		}
		elseif( $check === '1' ) {
			$EM_Locations 	= EM_Locations::get( $args, $count=false );
			$marker 		= array();
			$lats 			= array();
			$lngs 			= array();

			// Process individual locations.
			foreach( $EM_Locations as $EM_Location ) {
				$latitude 		= $EM_Location->location_latitude;
				$longitude 		= $EM_Location->location_longitude;
				$balloon 		= trim(preg_replace('/\s\s+/', '<br>', get_option('dbem_map_text_format')));
				if( $EM_Event ) {
					$balloon 	= $EM_Event->output($balloon);
				}
				$balloon 		= $EM_Location->output($balloon);
				$balloon 		= addslashes($balloon);
				$marker[] 		= "[\"{$balloon}\", {$latitude}, {$longitude}]";
				$lats[] 		= $latitude;
				$lngs[] 		= $longitude;
			}
			$markers 	= implode(", ", $marker);
			$high_lat 	= max($lats);
			$high_lng 	= max($lngs);
			$low_lat 	= min($lats);
			$low_lng 	= min($lngs);
			$avg_lat 	= array_sum($lats)/count($lats);
			$avg_lng 	= array_sum($lngs)/count($lngs);
			$mapbounds 	= '';
		}
		else {
			$EM_Locations 	= EM_Locations::get( $args, $count=false );
			$marker 		= array();
			$lats 			= array();
			$lngs 			= array();

			// Process individual locations.
			foreach( $EM_Locations as $EM_Location ) {
				$latitude 		= $EM_Location->location_latitude;
				$longitude 		= $EM_Location->location_longitude;
				$balloon 		= trim(preg_replace('/\s\s+/', '<br>', get_option('dbem_map_text_format')));
				if( $EM_Event ) {
					$balloon 	= $EM_Event->output($balloon);
				}
				$balloon 		= $EM_Location->output($balloon);
				$balloon 		= addslashes($balloon);
				$marker[] 		= "[\"{$balloon}\", {$latitude}, {$longitude}]";
				$lats[] 		= $latitude;
				$lngs[] 		= $longitude;
			}
			$markers 	= implode(", ", $marker);
			$high_lat 	= max($lats);
			$high_lng 	= max($lngs);
			$low_lat 	= min($lats);
			$low_lng 	= min($lngs);
			$avg_lat 	= array_sum($lats)/count($lats);
			$avg_lng 	= array_sum($lngs)/count($lngs);

			$mapbounds 	= "map.fitBounds([
					[{$high_lat}, {$high_lng}],
					[{$low_lat}, {$low_lng}]
				]);";
		}
		// Start the output.
		ob_start();
		?>
		<script>
			const zoomLevel = <?php echo $zoom; ?>;
			const avgLat = <?php echo $avg_lat; ?>;
			const avgLng = <?php echo $avg_lng; ?>;
			const highLat = <?php echo $high_lat; ?>;
			const highLng = <?php echo $high_lng; ?>;
			const lowLat = <?php echo $low_lat; ?>;
			const lowLng = <?php echo $low_lng; ?>;
			const locations = [<?php echo $markers; ?>];
			const myTiles = '<?php echo $mapType; ?>';
			const IconUrl = '<?php echo $marker_url; ?>';
			const IconShadow = '<?php echo $shadow_url; ?>';
		</script>
		<div id="em-osm-map-container">
			<div id="em-osm-map" style="width: <?php echo $width;?>; height: <?= $height; ?>;"></div>
			<?php
			wp_enqueue_style( 'em-osm-css', plugins_url('inc/em-osm.min.css', __FILE__));
			wp_enqueue_script( 'em-osm-core', plugins_url('inc/em-osm-core.min.js', __FILE__), array(), '', true);
			wp_enqueue_script('em-osm-multiple', plugins_url('inc/em-osm-multiple.min.js', __FILE__), array('em-osm-core'), '', true);
			?>
		</div>
		<?php
		$return  = ob_get_clean();
		return $return;
	}


	#===============================================
	# 	Create a hint to use the search fields.
	#===============================================
	public static function show_search_tip() {
		$expl 		= __('Hint', 'stonehenge-em-osm');
		$tooltip 	= __('If your location cannot be found, search for one nearby.<br>After the marker has been set, you can drag the marker to the preferred position and manually change the address details in the location form fields.', 'stonehenge-em-osm');

		$return 	= sprintf( '<div id="osm-search-tip" class="description"><span>%s %s</span></div>',
			__('The more details you provide, the more accurate the search result will be.', 'stonehenge-em-osm'),
			'<span class="osm-tooltip">('. $expl .')<span class="osm-tooltiptext">'. $tooltip .'</span></span>'
		);
		return $return;
	}


	#===============================================
	# 	Create WP Admin Menu item.
	#===============================================
	function add_submenu() {
		add_submenu_page(
			'edit.php?post_type='.EM_POST_TYPE_EVENT,
			'EM - OpenStreetMaps',
			'&#x1F5FA; OpenStreetMaps',
			'manage_options',
			'stonehenge-em-osm',
			array($this, 'show_options_page')
		);
	}


	#===============================================
	# 	Render Admin Settings Page.
	#===============================================
	function show_options_page() {
		$value 	= get_option('stonehenge-em-osm');
		echo '<div class="wrap">';
		echo '<h1>&#x1F5FA; Events Manager – OpenStreetMaps</h1>';
		echo sprintf( '<p>%s<br>%s</p>',
			__('This plugin completely replaces the built-in Google Maps with open source OpenStreetMap.', 'stonehenge-em-osm'),
			__('It uses OpenCage for geocoding.', 'stonehenge-em-osm')
		);

		if (isset($_GET['settings-updated'])) {
			echo '<div class="notice notice-success is-dismissible"><p>'. __('Your settings have been saved.', 'stonehenge-em-osm'). '</p></div>';
		}
		?>
		<form method="post" action="options.php" autocomplete="off">
		<?php settings_fields('stonehenge-em-osm');
		do_settings_sections('stonehenge-em-osm'); ?>
			<table class="form-table">
				<tr>
					<th><?php _e('OpenCage API Key', 'stonehenge-em-osm'); ?>:</th>
					<td><input type="text" id="stonehenge-em-osm[api]" name="stonehenge-em-osm[api]" class="regular-text" value="<?php echo @$value['api']; ?>" required>
					<?php
					echo sprintf( '<a href="%s" target="_blank">%s</a>',
						'https://opencagedata.com/pricing',
						'<button type="button" class="button-primary">'. __('Get your free key', 'stonehenge-em-osm') .'</button>'
					);
					echo sprintf('<p class="description">%s<br>%s</p>',
						__('A free OpenCage API Key has a daily limit of 2500 calls per day.', 'stonehenge-em-osm'),
						__('This plugin calls the OpenCage API if you:<br>- Create a new or edit an existing location.<br>- Use the "Search Location by Name" field in the Edit/Submit Event Page.', 'stonehenge-em-osm')
					);
					?>
					</td>
				</tr>
				<tr>
					<th><?php _e('Single Marker Zoom', 'stonehenge-em-osm'); ?>:</th>
					<td><input type="number" id="stonehenge-em-osm[zoom]" name="stonehenge-em-osm[zoom]" min="0" max="19" required="required" value="<?php echo @$value['zoom']; ?>">
						<p class="description"><?php _e('Enter a number between 1 and 19. (Default is 15)', 'stonehenge-em-osm'); ?></p>
					</td>
				</tr>
				<tr>
					<th><?php echo __('Marker Color:', 'stonehenge-em-osm'); ?></th>
					<td>
						<select id="stonehenge-em-osm[marker]" name="stonehenge-em-osm[marker]">
							<option value="blue" <?php selected(@$value['marker'], "blue" ); ?>><?php _e('Blue', 'stonehenge-em-osm');?></option>
							<option value="red" <?php selected(@$value['marker'], "red" ); ?>><?php _e('Red', 'stonehenge-em-osm');?></option>
							<option value="green" <?php selected(@$value['marker'], "green" ); ?>><?php _e('Green', 'stonehenge-em-osm');?></option>
							<option value="orange" <?php selected(@$value['marker'], "orange" ); ?>><?php _e('Orange', 'stonehenge-em-osm');?></option>
							<option value="yellow" <?php selected(@$value['marker'], "yellow" ); ?>><?php _e('Yellow', 'stonehenge-em-osm');?></option>
							<option value="violet" <?php selected(@$value['marker'], "violet" ); ?>><?php _e('Purple', 'stonehenge-em-osm');?></option>
							<option value="grey" <?php selected(@$value['marker'], "grey" ); ?>><?php _e('Grey', 'stonehenge-em-osm');?></option>
							<option value="black" <?php selected(@$value['marker'], "black" ); ?>><?php _e('Black', 'stonehenge-em-osm');?></option>
						</select>
					</td>
				</tr>
				<tr>
					<th><?php echo __('Map Style:', 'stonehenge-em-osm'); ?></th>
					<td><select id="stonehenge-em-osm[type]" name="stonehenge-em-osm[type]" value="<?php echo @$value['type']; ?>">
						<option value="//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" <?php selected(@$value['type'], "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"  ); ?>>OpenStreetMap</option>

						<option value="//{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png" <?php selected(@$value['type'], "//{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png" ); ?>>OpenStreetMap HOT</option>

						<option value="//server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}" <?php selected(@$value['type'], "//server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}" ); ?>>ArcGIS WorldMap</option>

						<option value="//server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}" <?php selected(@$value['type'], "//server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}" ); ?>>ArcGIS TopoMap</option>

						<option value="//server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}" <?php selected(@$value['type'], "//server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}" ); ?>>ArcGIS World Imagery</option>

						<option value="//{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png" <?php selected(@$value['type'], "//{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png" ); ?>>Hydda (max zoom = 18)</option>

					</select>
					<br>
					<p class="description"><?php _e('You can preview map styles by reloading any map on your website.', 'stonehenge-em-osm'); ?></p>
					</td>
				</tr>
			</table>
		<?php submit_button(); ?>
		</form>
		</div>
		<?php
	}


	#===============================================
	# 	Register & Update Plugin options to WordPress.
	#===============================================
	function register_settings() {
		register_setting('stonehenge-em-osm', 'stonehenge-em-osm', array($this, 'sanitize_options'));
	}


	#===============================================
	# 	Sanitize Plugin Options before saving to WordPress.
	#===============================================
	function sanitize_options( $input ) {
		$cleaned 			= array();
 		$cleaned['api'] 	= sanitize_text_field( $input['api'] );
		$cleaned['zoom'] 	= sanitize_text_field( $input['zoom'] );
		$cleaned['type'] 	= wp_kses_post( stripslashes($input['type']));
		$cleaned['marker']	= sanitize_key( $input['marker'] );
 		return $cleaned;
	}


} // End Class.

