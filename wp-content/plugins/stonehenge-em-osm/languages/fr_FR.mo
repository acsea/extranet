��    3      �  G   L      h  @   i     �     �     �  
   �     �     �  0   �  !   
     ,     >     D     I  �   N           @  (   O  	   x  
   �     �     �     �     �     �  .   �     �  L        S  	   c     m     t     x  5   �     �     �     �     �  J   �  -   @  -   n  �   �  m   M	     �	     �	  @   �	  %   
     ;
  0   Z
  #   �
     �
  e  �
  G   #     k     t     y     ~     �     �  8   �  !   �     �                   "  >   ?     ~  =   �     �     �     �     �               0  #   B     f  _   m     �     �     �     �     �  _        c     o     �     �  R   �  4     &   8  �   _  b   0     �     �  P   �  =   �  '   9  0   a  #   �     �     "                       %                    2       (   !                           .   /   1   ,                 +      	                          $   3   *   '                                
   -   0   &                        )   #                 A free OpenCage API Key has a daily limit of 2500 calls per day. Address: Black Blue City/Town: Country: Donate Enter a number between 1 and 19. (Default is 15) Events Manager – OpenStreetMaps Get your free key Green Grey Hint If your location cannot be found, search for one nearby.<br>After the marker has been set, you can drag the marker to the preferred position and manually change the address details in the location form fields. It uses OpenCage for geocoding. Location Name: Location coordinates could not be found. Location: Map Style: Map: Marker Color: No Location No locations found OpenCage API Key OpenStreetMaps replacement for Events Manager. Orange Please enter your OpenCage API Key in your <a href="%s">Plugin Settings</a>. Plug-in Support Postcode: Purple Red Region: Reset this form to create a location or search again. Settings Single Marker Zoom State/County: Stonehenge Creations The more details you provide, the more accurate the search result will be. This event does not have a location selected. This event does not have a physical location. This plugin calls the OpenCage API if you:<br>
				- create a new or edit an existing location.<br>
				- use the "Search Location by Name" field in the Edit/Submit Event Page. This plugin completely replaces the built-in Google Maps API with the free, open source Open Street Maps API. WordPress Profile Yellow You can preview map styles by reloading any map on your website. You cannot edit saved locations here. Your settings have been saved. https://wordpress.org/plugins/stonehenge-em-osm/ https://www.stonehengecreations.nl/ none selected Project-Id-Version: Events Manager – OpenStreetMaps v1.8.4
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2019-01-04 22:30+0100
Last-Translator: Patrick Buntsma <info@stonehengecreations.nl>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n>1;
X-Generator: Poedit 2.2
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Textdomain-Support: yes
Language: fr_FR
X-Poedit-SearchPath-0: .
 Une clé API OpenCage a une limite quotidienne de 2500 appels par jour. Adresse: Noir Bleu Ville: Pays: Faire un don Entrez un nombre compris entre 1 et 19. (15 par défaut) Events Manager – OpenStreetMaps Récupérer votre clé gratuite Vert Gris Aide Si votre emplacement n'est pas trouvé, cherchez-en un à proximité.<br>Après que le marqueur ait été positionné, vous pouvez le déplacer à la position initialement prévue à l'aide de la souris, et changer les détails de l'adresse manuellement dans les champs du formulaire. OpenCage est utilisé pour le codage géographique (geocoding) Nom de l'emplacement: Les coordonnées de l'emplacement ne peuvent être trouvées. Emplacement: Style de Carte: Carte: Couleur du marqueur: Pas d'emplacement Aucun lieu trouvé Clé API OpenCage OpenStreetMaps pour Events Manager. Orange Merci de renseigner votre clé API OpenCage <a href="%s">dans les réglages de l'extension</a>. Support du plug-in Code postal: Violet Rouge Région: Remettre à zéro ce formulaire pour créer un emplacement ou effectuer une nouvelle recherche. Paramètres Marqueur unique pour le zoom État/Région: Stonehenge Creations Plus vous fournissez de détails, plus précis seront les résultats de recherche. Cet évènement n'a pas d'emplacement sélectionné. Cet évènement n'a pas d'emplacement. Cette extension appelle l’API OpenCage si vous :<br>
				- créez ou modifiez un emplacement.<br>
				- utilisez la recherche d’emplacement par nom dans la page de création/modification d’évènement. Ce plugin remplace complètement Google Maps par l'alternative Libre et Open Source OpenStreetMap. Profil WordPress Jaune Vous pouvez pré-visualiser les styles de vos cartes en rechargeant n’importe. Vous ne pouvez pas éditer des emplacements enregistrés ici. Vos paramètres ont été sauvegardés. https://wordpress.org/plugins/stonehenge-em-osm/ https://www.stonehengecreations.nl/ aucune sélection 