��    3      �  G   L      h  @   i     �     �     �  
   �     �     �  0   �  !   
     ,     >     D     I  �   N           @  (   O  	   x  
   �     �     �     �     �     �  .   �     �  L        S  	   c     m     t     x  5   �     �     �     �     �  J   �  -   @  -   n  �   �  m   E	     �	     �	  @   �	  %   
     3
  0   R
  #   �
     �
  ;  �
  E   �     7     >     D     J     R     X  4   `  !   �     �     �     �     �  �   �  '   �     �  1   �     )  
   2     =     D     \     i       .   �     �  [   �     &  	   ;     E     K  
   P  H   [     �     �  
   �     �  H   �  0   6  "   g  �   �  l   c     �     �  k   �  /   S      �  0   �  #   �     �     "                       %                    2       (   !                           .   /   1   ,                 +      	                          $   3   *   '                                
   -   0   &                        )   #                 A free OpenCage API Key has a daily limit of 2500 calls per day. Address: Black Blue City/Town: Country: Donate Enter a number between 1 and 19. (Default is 15) Events Manager – OpenStreetMaps Get your free key Green Grey Hint If your location cannot be found, search for one nearby.<br>After the marker has been set, you can drag the marker to the preferred position and manually change the address details in the location form fields. It uses OpenCage for geocoding. Location Name: Location coordinates could not be found. Location: Map Style: Map: Marker Color: No Location No locations found OpenCage API Key OpenStreetMaps replacement for Events Manager. Orange Please enter your OpenCage API Key in your <a href="%s">Plugin Settings</a>. Plug-in Support Postcode: Purple Red Region: Reset this form to create a location or search again. Settings Single Marker Zoom State/County: Stonehenge Creations The more details you provide, the more accurate the search result will be. This event does not have a location selected. This event does not have a physical location. This plugin calls the OpenCage API if you:<br>
- create a new or edit an existing location.<br>
- use the "Search Location by Name" field in the Edit/Submit Event Page. This plugin completely replaces the built-in Google Maps API with the free, open source Open Street Maps API. WordPress Profile Yellow You can preview map styles by reloading any map on your website. You cannot edit saved locations here. Your settings have been saved. https://wordpress.org/plugins/stonehenge-em-osm/ https://www.stonehengecreations.nl/ none selected Plural-Forms: nplurals=2; plural=(n != 1);
Project-Id-Version: Events Manager – OpenStreetMaps
POT-Creation-Date: 2019-01-04 22:35+0100
PO-Revision-Date: 2019-01-04 22:38+0100
Language-Team: Patrick Buntsma <info@stonehengecreations.nl>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2
X-Poedit-Basepath: ..
X-Poedit-Flags-xgettext: --add-comments=translators:
X-Poedit-WPHeader: stonehenge-em-osm.php
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Last-Translator: Patrick Buntsma <info@stonehengecreations.nl>
Language: nl_NL
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Een gratis OpenCage API-sleutel heeft een dagelijkse limiet van 2500. Adres: Zwart Blauw Plaats: Land: Doneren Voer een nummer tussen 1 en 19 in. (Standaard is 15) Events Manager – OpenStreetMaps Haal je gratis sleutel op Groen Grijs Hint Als je locatie niet gevonden kan worden, zoek dan naar een in de buurt.<br>Als de markering geplaatst is, kun je de markering naar de gewenste positie slepen en handmatig de adresdetails in de locatie-invulvelden aanpassen. Het gebruikt OpenCage for geo-codering. Locatienaam: Locatie-coördinaten konden niet gevonden worden. Locatie: Kaarttype: Kaart: Kleur van de Markering: Geen locatie Locatie niet gevonden OpenCage API-sleutel OpenStreetMaps vervanging voor Events Manager. Oranje Voer alstublieft je OpenCage API-sleutel in je <a href=“%s”>Plugin Instellingen</a> in. Plugin Ondersteuning Postcode: Paars Rood Provincie: Reset dit formulier om een locatie te aan te maken of opnieuw te zoeken. Instellingen Enkelvoudige Markering Zoom Provincie: Stonehenge Creations Hoe meer details je invult, hoe nauwkeuriger het zoekresultaat zal zijn. Voor dit evenement is geen locatie geselecteerd. Geen adres voor deze collegereeks. Deze plug-in roept de OpenCage API aan wanneer u:<br>
- een nieuwe locatie aanmaakt of een bestaande locatie bewerkt.<br>
- het “Zoek Locatie op Naam”-veld in de “Evenement bewerken/inzenden”-pagina gebruikt. Deze plugin vervangt volledig de ingebouwde Google Maps API met de gratis, open source Open Street Maps API. WordPress-profiel Geel Je kunt voorbeelden van de kaarttypes bekijken door elke willekeurige kaart op je website opnieuw te laden. Je kunt hier opgeslagen locaties niet bewerken. Je instellingen zijn opgeslagen. https://wordpress.org/plugins/stonehenge-em-osm/ https://www.stonehengecreations.nl/ niets geselecteerd 