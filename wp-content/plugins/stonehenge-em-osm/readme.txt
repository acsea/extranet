=== Events Manager – OpenStreetMaps ===

Plugin Name: Events Manager – OpenStreetMaps
Contributors: DuisterDenHaag
Tags: Events Manager, Maps, Open, Street, Google, free, map
Donate link: https://useplink.com/payment/VRR7Ty32FJ5mSJe8nFSx
Requires at least: 4.9
Tested up to: 5.0.2
Requires PHP: 7.1
Stable tag: 1.8.5.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

OpenStreetMaps for Events Manager. Replace Google Maps with OpenStreetMap. 100% free.


== Description ==
**0% Google, 100% open source.**
Use the free and open source OpenStreetMap to display your Events Manager Location Maps.

Existing locations should work right out-of-the-box.

This plugin completely replaces the original Google Maps API (paid) with the open-source OpenStreetMap API. Once installed and activated, this plugin will automatically disable the Google Maps integration in Events Manager for you and replace it with OpenStreetMaps.

**You only need one, free OpenCage API key.**
It has a daily limit of 2500 calls per day, so you should be okay, because it is only called when adding or editing a location or event.

NOTE:
This might change in the near future. I will keep you posted on any changes and why.


== Localisation ==
* US English (default)
* Dutch (included)
* French (included)
* German (included)

The plugin is ready to be translated, all texts are defined in the POT file. Any contributions to localise this plugin are very welcome!


== Feedback ==
I am open to your suggestions and feedback!
[Please also check out my other plugins.](https://profiles.wordpress.org/duisterdenhaag#content-plugins)


== Frequently Asked Questions ==
= What is the big benefit of OpenStreetMaps? =
**1)** OpenStreetMaps use a free, open source platform.
Check their website for more information: [OpenStreetMap.org](https://www.openstreetmap.org/about).

**2)** This plugin with OpenStreetMap will <b><i>not</i></b> request any visitor (location) info to display the map. So, that makes it easier to include in your own GDPR compliance.

**3)** This plugin only requires one, free API Key, provided by [OpenCageData.com](https://www.OpenCageData.com).


= Why are Map Sizes different? =
The maps shown in the meta boxes <em>(Add/Edit Location & Event in the back-end and the front-end submission forms)</em> have fixed dimensions of 400px X 300px (EM default) to ensure a correct display of the meta boxes.

You can set the dimensions for #_LOCATIONMAP (single location) in Events &rarr; Settings &rarr; Formatting &rarr; Maps.
These will also be the default dimension for the [locations_map]. If you wish to display the [locations_map] differently, you can set those dimensions from within the shortcode: [locations_map width="500px" height="500px"].


= Why is my map not visible? =
You probably have set your map dimension in percentages (100%). Please check Events &rarr; Settings &rarr; Formatting &rarr; Single Event Page &rarr; Single Event Page format.
Replace: `<div style="float:right; margin:0px 0px 15px 15px;">#_LOCATIONMAP</div>`

With: `#_LOCATIONMAP`

Because the div has no width set, it is automatically scaled to 0px. Therefore your map is filling 100% of 0px.

**All EM OpenStreetMaps are being wrapped in a div.**
You can target that with custom css in your stylesheet to best suit your theme's responsiveness. "#em-osm-map-container {}"

== Installation ==
1. First make sure the original Events Manager plugin is installed and activated.
2. Install and activate this plugin.
3. Upon activation this plugin will automatically disable the Google Maps integration in Events Manager for you.
4. Enter your free OpenCage API key and preferred settings in the options page.
5. Enjoy the free OpenStreetMaps on your website.


== Screenshots ==
1. Single Location Map.
2. Multiple Locations Map.
3. Select Map Style and Marker Color.
4. Edit Event Page (Front-End Submission).
5. Settings Page (Admin area).


== Upgrade Notice ==


== Changelog ==
= 1.8.5.1 =
- Minor bugfix that caused a PHP error.

= 1.8.5 =
- Added support for shortcodes: [location], [event] & [events_map].
- Bugfix Plugin Dependency check.
- Updated the method which checks if the correct template files are used, to prevent PHP warnings.
- Updated the method for template files when switching themes.
- Updated .pot file so new strings are translatable.
- Updated the loading of included translation files.
- Corrected the OpenStreetMap naming and branding.
- Code clean-up.

= 1.8.4 =
- Bugfix: [locations_map] loaded the wrong balloon content.
- Bugfix: Changed get_template_directory() to get_stylesheet_directory() for template files in Child Themes.
- Bugfix: Removed a console error when creating a new location through the Event Page.
- Added German translation. Thanks, Michael.

= 1.8.3 =
- Fixed loading of template files and correct wrong functions.

= 1.8.2. =
- Corrected a typo in the map width, making it 0px.

= 1.8.1 =
- Minor bugfix that gave warning notices for the new options in the settings page.
- Minor bugfix that made every map a fixed size.

= 1.8.0 =
- Added option to select different Map Styles.
- Added option to select different Marker Colors.
- Fixed conflict with Auto Optimize and other caching plugins: javaScript is now minimized and loaded in the footer.
- All images (except Map Tiles) used by this plugin have been optimized.
- Removed option to zoom to marker on a map with multiple locations.

= 1.7.5 =
- Made sure coordinates are now correctly saved and retrieved in Edit Event Page.

= 1.7.4 =
- Added forgotten file for Location Select Dropdown. (Oops!) ;-)

= 1.7.3. =
- Added minified versions of used javascript.
- Fixed a bug that prevented to replacement of required template files.
- Optimized images for faster loading.

= 1.7.2 =
- Solved an indexing error that prevented all Location fields to be filled properly when editing a location or event.

= 1.7.1 =
- Minor bugfix that resulted in every search result being in French.

= 1.7.0 =
- **Re-introduced the use of the OpenCage API key.** See Plugin Description for more info.
- Improved handling of maps on font-end submission forms.
- Added option to zoom (or not) on maps with multiple markers.
- Reverted back to OSM native map tiles: they provide more detail.
- Improved use of EM template files, making it easier to edit and extend in the future.
- Updated Frequently Asked Questions.
- Updated screenshots.

= 1.6.1 =
- Added activation & deactivation hooks: restoring WordPress back to Google Maps if this plugin is deactivated.

= 1.6.0 =
- Added support for front-end event & location submission forms.
- Added support for conditional placeholders in balloon texts.
- Added "preconnect" in wp_head for faster loading.
- Changed Map Tiles provider for faster loading.
- All maps are now conveniently wrapped in a non-styled div (#em-osm-map-container) for easier targeting by css.
- Updated the translation files.
- Some minor coding improvements.

= 1.5.1 =
- Added extra processors for balloon text. If a user had used hard enters (new lines) in the textareas for the balloon text in the EM settings, they broke the JavaScript code.

= 1.5 =
- Added support for front-end location submission.
- Switches the map and form fields around, so the layout is more logical (especially when editing in the front-end).
- Updated Dutch translation file.

= 1.4 =
- Improved processing of [locations_map] shortcode.
- Added support for arguments such as [locations_map scope="future"].

= 1.3 =
- Removed the need for an OpenCage API key.
- Completely revised the way an address is looked up.
- Revised scripts to display at the front-end.
- Uploaded new screenshots.
- Updated this ReadMe file.

= 1.2 =
- Changed the way the maps are shown. The Map should now remain in the assigned div.
- Changed the way the zoom level was called.

= 1.1 =
First public release through the WordPress Repository.

