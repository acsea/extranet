let map = L.map('em-osm-map', {
	center: [avgLat, avgLng],
	zoom: zoomLevel
	});

L.tileLayer(myTiles, {
	attribution: '&copy; <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">OpenStreetMap</a> Contributors',
	minZoom: 1,
	maxZoom: 19,
}).addTo(map);


map.fitBounds([
		[highLat, highLng],
		[lowLat, lowLng]
	]);

map.addControl(new L.Control.Fullscreen({
	position: 'topright',
}));


const customIcon = new L.Icon({
  iconUrl: IconUrl,
  shadowUrl: IconShadow,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  shadowSize: [41, 41]
});

//	let marker = L.marker([Lat, Lng], {draggable: 'true', icon: customIcon},).addTo(map).bindPopup(balloon).openPopup();


for (var i = 0; i < locations.length; i++) {
	marker = new L.marker([locations[i][1],locations[i][2]], {icon: customIcon},).addTo(map).bindPopup(locations[i][0]);
}


function clickZoom(e) {
	map.setView(e.target.getLatLng(),zoomLevel);
}

setTimeout(function(){ map.invalidateSize()}, 400);
