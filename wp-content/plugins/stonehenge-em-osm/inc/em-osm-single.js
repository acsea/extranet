setTimeout(function() {
    map.invalidateSize()
}, 400);
let map = L.map('em-osm-map', {
    center: [Lat, Lng],
    zoom: zoomLevel
});
L.tileLayer(myTiles, {
    attribution: '&copy; <a href=\"https://www.openstreetmap.org/copyright\" target=\"_blank\">OpenStreetMap</a> Contributors',
    minZoom: 1,
    maxZoom: 19,
}).addTo(map);
const customIcon = new L.Icon({
    iconUrl: IconUrl,
    shadowUrl: IconShadow,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
});
let marker = L.marker([Lat, Lng], {
    draggable: 'true',
    icon: customIcon
}, ).addTo(map).bindPopup(balloon).openPopup();
marker.on('dragend', function(e) {
    document.getElementById('location-latitude').value = marker.getLatLng().lat;
    document.getElementById('location-longitude').value = marker.getLatLng().lng;
});
map.addControl(new L.Control.Fullscreen({
    position: 'topright',
}));

function searchAddress() {
    var addr = document.getElementById('location-address').value;
    var city = document.getElementById('location-town').value;
    var pcode = document.getElementById('location-postcode').value;
    var state = document.getElementById('location-state').value;
    geoAddress(addr + ', ' + city + ', ' + pcode + ', ' + state);
}

function geoAddress(query) {
    jQuery.ajax({
        url: 'https://api.opencagedata.com/geocode/v1/json',
        method: 'GET',
        data: {
            'key': api,
            'q': query,
            'no_annotations': 1,
            'add_request': 0,
            'pretty': 1,
            'language': locale,
        },
        dataType: 'json',
        statusCode: {
            200: function(response) {
                var result = response.results[0];
                var formatted = result.formatted;
                var country = result['components']['country'];
                formatted = formatted.replace(', ' + country, '');
                var components = result['components']
                if (components.hasOwnProperty('city')) {
                    document.getElementById('location-town').value = result['components']['city'];
                }
                if (components.hasOwnProperty('town')) {
                    document.getElementById('location-town').value = result['components']['town'];
                }
                document.getElementById('location-state').value = result['components']['state'];
                if (components.hasOwnProperty('county')) {
                    document.getElementById('location-region').value = result['components']['county'];
                }
                if (components.hasOwnProperty('postcode')) {
                    document.getElementById('location-postcode').value = result['components']['postcode'];
                }
                var countryCode = result['components']['country_code'];
                countryCode = countryCode.toUpperCase();
                document.getElementById('location-country').value = countryCode;
                document.getElementById('location-latitude').value = result['geometry']['lat'];
                document.getElementById('location-longitude').value = result['geometry']['lng'];
                var newLat = result['geometry']['lat'];
                var newLng = result['geometry']['lng'];
                var newLatLng = new L.LatLng(newLat, newLng);
                newName = document.getElementById('location-name');
                if (newName && newName.value) {
                    newBalloon = document.getElementById('location-name').value;
                } else {
                    newBalloon = formatted;
                }
                marker.setLatLng(newLatLng).bindPopup(newBalloon);
                map.setView(newLatLng, setZoom);
            },
            402: function() {
                console.log('hit free-trial daily limit');
                console.log('become a customer: https://opencagedata.com/pricing');
            }
        }
    });
}

function searchCoords() {
	// Only execute if Ajax Search is performed.
	if( jQuery("#location-address").val() ) {
		var addr = document.getElementById('location-address').value;
		var city = document.getElementById('location-town').value;
		var pcode = document.getElementById('location-postcode').value;
		var state = document.getElementById('location-state').value;
		geoCoords(addr + ', ' + city + ', ' + pcode + ', ' + state);
	}
}

function geoCoords(query) {
    jQuery.ajax({
        url: 'https://api.opencagedata.com/geocode/v1/json',
        method: 'GET',
        data: {
            'key': api,
            'q': query,
            'no_annotations': 1,
            'add_request': 0,
            'pretty': 1,
            'language': locale,
        },
        dataType: 'json',
        statusCode: {
            200: function(response) {
                var result = response.results[0];
                var newLat = result['geometry']['lat'];
                var newLng = result['geometry']['lng'];
                var newLatLng = new L.LatLng(newLat, newLng);
                newBalloon = document.getElementById('location-name').value;
                marker.setLatLng(newLatLng).bindPopup(newBalloon);
                map.setView(newLatLng, setZoom);
                jQuery("#location-latitude").val(newLat);
                jQuery("#location-longitude").val(newLng);
            }
        }
    });
}