<?php /*Template Name: Formulaire de création chat spécifique*/;?>
<?php acf_form_head(); ?>
<?php get_header(); ?>

<div id="container" class="col-xl-12 ask-form" >
	<div class="ct-container">
	    <div class="row">
	    	<div class="col-sm-12">

			<?php /* The loop */ ?>
						<?php /* The loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
				
				<div class="ct-title">
				<h2><?php the_title(); ?></h2>
   	     			<div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
				</div>
				<?php the_content(); ?>
				<!------------>
		
				<?php  $options = array(
					'post_id'				=> 'new_chat_specific',
					'field_groups' 			=> array(3853),
					'post_title'			=> false,
					'post_type'				=> 'chat',
					'post_status'			=> 'draft',
			        'return' => '?my_param=true&updated=true',
					'submit_value'			=> 'Créer le chat',
					'html_updated_message'	=> "<div class='alert alert-success'>Votre demande a été envoyé avec succès ! Toutefois, elle nécessite une intervention de l'administrateur afin d'ajouter les membres du chat. Si ce dernier n'apparait pas au bout de 24h, n'hésitez pas à contacter l'administateur.</div>",
				);
				 acf_form($options);
				 ?>

			<?php endwhile; ?>
			</div>
		</div><!-- #content -->
	</div><!-- #primary -->
</div>

<?php get_footer(); ?>