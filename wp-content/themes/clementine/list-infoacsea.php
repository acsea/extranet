<?php 
/* Template Name: Info ACSEA - Liste */
get_header();
?>
<div class="row col-xl-12">
    <div class="col-xl-12">
        <div class="ct-container ct-gallery col-xl-12">
            <?php 
                include "template-parts/header_page.php";
            ?>

            <div class="row col-xl-12 list-infoacsea list-item">
            <?php echo do_shortcode('[ajax_load_more id="1059056237" css_classes="list-infoacsea" transition_container_classes="row col-xl-12" posts_per_page="8" container_type="div" repeater="template_2" post_type="info_acsea"]'); ?>
            </div>
            <?php 
                get_footer(); 
            ?>
        </div>  
    </div>
</div>
<?php get_sidebar(); ?>