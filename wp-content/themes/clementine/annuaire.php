<?php 
/* Template Name: Annuaire */
get_header(); 
$args = array(
    'post_type' 		=> 'etablissement',
    'posts_per_page' 	=> 100,
);
?>
<div class="row col-xl-12">
  	<div class="col-xl-12">
    	<div class="ct-container ct-directory col-xl-12">
      		<div class="ct-title">
        		<h2><?php the_title(); ?></h2>
        		<span><?php the_excerpt(); ?></span>
        		<div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
      		</div>
      		<br>
      		<div class="row col-xl-12 justify-content-between">
      			<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<div class="scrollable-table">
							<table class="tftable gfg order-table" border="0" id="table-id">
								<thead class="tr-head">
									<th>Nom</th>
									<th>Adresse</th>
									<th>Ville</th>
									<th>Mail</th>
									<th>Téléphone</th>
									<th>Pôle</th>
								</thead>
								<div class="an-filters">
									<div class="fi-pole">
										<h4>Filtrer par pôle</h4>
										<ul id="filter-options">
										  	<li><input type="checkbox" value="protection-enfance-famille" data-filter_id="pef"> Protection Enfance Famille</li>
										  	<li><input type="checkbox" value="handicap-autonomie-dependance" data-filter_id="had"> Handicap Autonomie Dépendance</li>
										  	<li><input type="checkbox" value="prevention-et-lutte-contre-les-exclusions" data-filter_id="plce"> Prévention et lutte contre les exclusions</li>
										  	<li><input type="checkbox" value="sante" data-filter_id="sante"> Sante</li>
										</ul>
									</div>
									<div class="fi-name">
										<h4>Rechercher un établissement</h4>
										<input type="search" class="search-field an-search light-table-filter" data-table="order-table" placeholder="Filter">
									</div>
								</div>
								<tbody class="an-list" id="ets-list">
									<?php 
									$query 		= 	  new WP_Query( $args );
									if ( $query->have_posts() ) {
									    // Start looping over the query results.
									    while ( $query->have_posts() ) {
									 		$query 		->		the_post();
											$adresse 	= 		get_field( "adresse" );
											$zipcode 	= 		get_field( "code_postal" );
											$city 		= 		get_field( "ville" );
											$telephone 	= 		get_field( "numero_de_telephone_1" );
											$mail 		= 		get_field( "adresse_mail" );
											$pole 		= 		get_field_object('field_5c92457756909');
											$pole2 		= 		$pole['value'][0];
									?> 


									<tr class="<?php echo $pole2->slug; ?>">
										<td class="an-name <?php echo $pole2->slug; ?>"><b><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></b></td>
										<td class="an-adress"><?php echo $adresse; ?></td>
										<td class="an-city"><?php echo $city; ?></td>
										<td class="an-mail"><?php echo $mail; ?></td>
										<td class="an-phone"><?php echo $telephone; ?></td>
										<td class="an-pole"><?php echo $pole2->name; ?></td>
									</tr>
									<?php
									    }
									 }
									wp_reset_postdata();
									?>
								</tbody>
							</table>
						</div>
					</main>
				</div>
			</div>
      	</div>
    </div>  
</div>

<?php
get_sidebar();
get_footer();