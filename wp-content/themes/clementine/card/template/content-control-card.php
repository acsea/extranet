        <div class="col-lg-6 col-xl-4">
          <article id="post-<?php the_ID(); ?>" class="col-lg-12">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
              <div class="ct-inner-img">
                <img src="<?php echo $photo ?>">
                <span class="btn status-card <?php echo $status?>"><?php echo $status_text ?></span>
              </div>
            </a>
            <div class="box-meta">
              <h3>
                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                  <?php the_title(); ?>
                </a> 
              </h3>
              <?php the_excerpt(); ?>           
            </div>
          </article>
        </div>