<?php 

get_header(); 
    
?>
<div class="row col-xl-12">
  <div class="col-xl-3">
    <div class="ct-container col-xl-12 ct-inner-sidebar">
      <h2 class="ct-title">Personnel</h2>
      <?php echo do_shortcode('[subpages depth="1" childof="1244"]'); ?>

    </div>    
  </div>
  <div class="col-xl-9">
    <div class="ct-container ct-gallery col-xl-12">
      <div class="ct-title">
        <h2><?php the_title(); ?></h2>
        <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 

      </div>
      <br>
      <div class="row col-xl-12">    