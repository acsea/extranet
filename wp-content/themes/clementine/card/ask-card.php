<?php /*Template Name: Ask Card*/;?>
<?php get_header(); ?>

<div class="col-xl-12">
	<div class="ct-container">
		<form name="ets-form" id="ets-form" method="POST" action="" onchange="changeAction(this)">
			<div class="ct-title">
				<h2>
					<label for="ets-select">De quel établissement faites vous parti ? </label>
				</h2>
		        <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 	
			</div>
					<div class="clm-select">
				<select name="choose-ets" id="ets-select" class="minimal">
				    <option value="">--Choisir--</option>

				    <option value="dmo/">Action Éducative en Milieu Ouvert (AEMO)</option>
				    <option value="dmo/">Service Éducatif en Milieu Ouvert (SEMO)</option>
				    <option value="dmo/">Service de Protection en Milieu Ouvert (SPMO)</option>
				    <option value="dmo/">Service d'Investigation, de Médiation et d'Activités Pénales (SIMAP)</option>
					<option value="dafhe/">Foyers Éducatifs</option>
				    <option value="dafhe/">Service de Placement Familial Spécialisé (SPFS)</option>

				    <option value="ime/">Institut Médico Éducatif (IME) L'Espoir</option>
				    <option value="champ-goubert/">Institut de Champ Goubert - Institut Thérapeutique Educatif et Pédagogique</option>
				    <option value="champ-goubert/">Centre d'Accueil Familial Spécialisé (CAFS)</option>
				    <option value="impro-sessad/">Institut Médico-Professionnel (IMPRO) de Démouville</option>
				    <option value="impro-sessad/">Service d’Éducation Spécialisée et de Soins À Domicile (SESSAD)</option>
				    <option value="icb/">Institut Camille Blaisot (ICB)</option>
				    <option value="dahad/">Foyer de Vie Le Montmirel</option>
				    <option value="dahad/">Maison d'Accueil Spécialisée (MAS)</option>



				    <option value="defi/">ACSEA Formation</option>
				    <option value="defi/">Entreprise Adaptée Conchylicole (EAC)</option>
				    <option value="defi/">Entreprise Adaptée Restauration (EAR)</option>
				    <option value="defi/">Établissement et Service d'Aide par le Travail Hors Les Murs (ESAT)</option>
				    <option value="satc/">Service ATC (SATC)</option>
				    <option value="dip/">Service Trait d'Union (STDU)</option>
				    <option value="dip/">Service d'Aide aux Jeunes en Difficulté (SAJD)</option>
				    <option value="dip/">Service d'Action Préventive (SAP)</option>

				    <option value="dps/">Maison des Adolescents (MDA)</option>
				    <option value="dps/">Centre de Guidance (CMPP / BAPU / CAMSP)</option>


				</select>
			</div>
			<input class="btn padding10-20" type="submit" value="Continuer">
		</form>	
	</div>
</div>

<script type="text/javascript">
changeAction = function(select){
   document.getElementById("ets-form").action = document.getElementById("ets-select").value;
}
</script>

<?php 
get_footer(); ?>