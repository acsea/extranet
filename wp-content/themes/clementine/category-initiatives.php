<?php
/**
 * Page : Initiatives
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 * 
 */

get_header();
$categories     =     get_the_category();
$category_name  =     $categories[0]->name;

?>
<div class="col-xl-12">
    <div class="ct-init row justify-content-md-center">
        <?php include 'breadcrumb.php'; ?>
        <div class="col-xl-3 d-flex justify-content-center flex-column">
            <h2><?php echo $category_name ?></h2>
            <p>La section initiatives met en avant les projets solidaires portés par les membres de l'association.</p>
        </div>
        <div class="ct-container ct-featured row col-xl-9">
            <?php echo do_shortcode('[ajax_load_more id="3894821851" container_type="div" post_type="post" posts_per_page="3"  transition_container_classes="row col-xl-12" repeater="template_4" scroll_container=".ct-featured" category="initiatives"]'); ?>
        </div>
    </div>
</div>
<?php
get_sidebar();
get_footer();
