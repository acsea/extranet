	
jQuery(window).resize(function(){
	if ($(window).width() > 577) {
	   		$(".hd-menu").hide();
			$("body").removeClass('nav-need-help');
			$(".hidder2").hide();
			// $(".hd-menu").detach().appendTo('body');
	}
		if ($(window).width() < 678) {
	   		// $("html").removeClass("sidebar-enabled");
	   		// $("body").removeClass("sidebar-enabled");
	   		// $(".widget-area, .hidder").hide();
			// $(".hd-menu").detach().appendTo('body');
	}
})

// jQuery(window).scroll(function() {
//     if ($(window).scrollTop() == $(document).height() - $(window).height()) {
//     	alert("ok");
//     	var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
//         var data = {
//             'action': 'load_posts_by_ajax',
//             'page': page,
//             'security': '<?php echo wp_create_nonce("load_more_posts"); ?>'
//         };
 
//         $.post(ajaxurl, data, function(response) {
//             if(response != '') {
//                 $('.infoacsea-article').append(response);
//                 page++;
//             } else {
//                 $('.loadmore').hide();
//
//             }
//         });
//     }
// });

	jQuery( document ).ready(function($) {

	$('.em-booking-submit').addClass('btn');
	$('#em-booking p a').addClass('btn');
	$("#dbem-bookings-table td:contains('Approuvée')").wrapInner("<span class='approved'></span>");
	$("#dbem-bookings-table td:contains('Rejetée')").wrapInner("<span class='rejected'></span>");
	$("#dbem-bookings-table td:contains('En attente')").wrapInner("<span class='pending'></span>");
	$('#em-booking p a').css("color","white!important");
	$('.search-box .button, #post-query-submit, #post-query-submit.button-secondary, .em-button').addClass('btn');
	$("td.an-pole").empty();
	$(".infoacsea-article").hover(function(){
		$(this).find('.box-meta').fadeToggle( 500, "linear" );
	});
	$('li.menu-item:nth-child(1)').addClass('test');
	$('li.menu-item').click(function() {
	  $(this).children('.test').trigger('click');
	});
	$('.box-meta h3:contains("Info ACSEA")').each(function(){
    jQuery(this).html(jQuery(this).html().split("Info ACSEA").join(""));
});
	$(".page-template-formations .clear").remove();
	$('.ticket-spaces-min label').text('Places minimum par réservation');
	$('.ticket-spaces-max label').text('Places maximum par réservation');
      $('.em-bookings-dashboard').find('h2').replaceWith(function() {
                return '<h3>' + $(this).text() + '</h3>';
      });
	$("#filter-options :checkbox").click(function() 
	{
       	$("#ets-list tr").hide();
       	$("#filter-options :checkbox:checked").each(function() 
       	{
           $("." + $(this).val()).fadeIn();
		});
       
        if($('#filter-options :checkbox').filter(':checked').length < 1) 
        {
        	$("#ets-list tr").fadeIn();
        	
        }
        
    });

/*--------------------------------------------------------------
# in-wrapper des a de premiers niveaux avec un span
--------------------------------------------------------------*/

		$('li.dropdown a:first-of-type').wrapInner("<span class='fix-menu'></span>");
		$('.sub-menu span').removeClass("fix-menu");
		$('.sub-menu span').addClass("inner-menu");


/*--------------------------------------------------------------
# Ajouter des attr aux items de menu 
--------------------------------------------------------------*/

        $('#menu-item-3619').find('a').attr('data-toggle', 'modal');
        $('#menu-item-3619').find('a').attr('data-target', '#helpModal');


/*--------------------------------------------------------------
# Menu RH, menu CCE
--------------------------------------------------------------*/

		$("#menu-rh, #menu-cce, #menu-syndicat").addClass("row");
		$("#menu-rh li, #menu-cce li, #menu-syndicat li").wrap("<div class='col-xl-6'></div>");

/*--------------------------------------------------------------
# Debugger la modal assitance + prefill des champs
--------------------------------------------------------------*/
 	
		$('.tutorial').appendTo("body");
		$('#helpModal').appendTo("body");
		$('#helpModal .your-name input').val(  $('.hd-left .name').text() );

		$('#helpModal .your-email input').val(  $('#myMail').attr("value") );


/*--------------------------------------------------------------
# Agenda shortcode sur home translation
--------------------------------------------------------------*/

$("#ect-no-events p").text("Il n'y a pas d'événement prévu pour le moment.");

/*--------------------------------------------------------------
# Single établissement
--------------------------------------------------------------*/

$(".acf-field-5c471361ce95a label, .acf-field-5c47132e0f524 label").addClass("h2");

// $("#acf-field_5c46fe1a4e7a4-Lundi").change(function() {
//     if(this.checked) {
//         $(".acf-field-5c46fd5d3c8ea, .acf-field-5c46fe919ae7e").toggle();
//     } else {
//     	$(".acf-field-5c46fd5d3c8ea, .acf-field-5c46fe919ae7e").toggle();
//     }
// });

// $("#acf-field_5c46fe1a4e7a4-Mardi").change(function() {
//     if(this.checked) {
//         $(".acf-field-5c4713b66668b, .acf-field-5c471447b6633").toggle();
//     } else {
//         $(".acf-field-5c4713b66668b, .acf-field-5c471447b6633").toggle();
//     }
// });

// $("#acf-field_5c46fe1a4e7a4-Mercredi").change(function() {
//     if(this.checked) {
//         $(".acf-field-5c4713b56668a, .acf-field-5c471447b6632").toggle();
//     } else {
//         $(".acf-field-5c4713b56668a, .acf-field-5c471447b6632").toggle();
//     }
// });

// $("#acf-field_5c46fe1a4e7a4-Jeudi").change(function() {
//     if(this.checked) {
//         $(".acf-field-5c471446b6631, .acf-field-5c4713b466689").toggle();
//     } else {
//         $(".acf-field-5c471446b6631, .acf-field-5c4713b466689").toggle();
//     }
// });

// $("#acf-field_5c46fe1a4e7a4-Vendredi").change(function() {
//     if(this.checked) {
//         $(".acf-field-5c471446b6630, .acf-field-5c4713b466688").toggle();
//     } else {
//         $(".acf-field-5c471446b6630, .acf-field-5c4713b466688").toggle();
//     }
// });

// $("#acf-field_5c46fe1a4e7a4-Samedi").change(function() {
//     if(this.checked) {
//         $(".acf-field-5c4713b366687, .acf-field-5c471446b662f").toggle();
//     } else {
//         $(".acf-field-5c4713b366687, .acf-field-5c471446b662f").toggle();
//     }
// });

// $("#acf-field_5c46fe1a4e7a4-Dimanche").change(function() {
//     if(this.checked) {
//         $(".acf-field-5c4713b266686, .acf-field-5c471445b662e").toggle();
//     } else {
//         $(".acf-field-5c4713b266686, .acf-field-5c471445b662e").toggle();
//     }
// });




/*--------------------------------------------------------------
# Réinitialiser le mot de passe
--------------------------------------------------------------*/

$("#reinitPassword").on('click', function(){    // 2nd (A)
	$("#connectForm").toggle();
	$("#connectReinit").toggle();

});

$(".wppb-success").addClass("alert");
$(".wppb-success").addClass("alert-success");



/*--------------------------------------------------------------
# Soumettre un changement établissement
--------------------------------------------------------------*/

   $('.ets-edit-box .your-name input').val(  $('.hd-left .name').text() );
   $('.ets-edit-box .your-email input').val(  $('#myMail').attr("value") );
   $('.etablissement-template .ets-edit-box .your-subject input').val(  $('.ct-info h2').text() ); 
	$("#submitChange").on('click', function(){    // 2nd (A)
		$(".acf-map").toggle();
		$(".ets-edit-box").toggle();
	});

// $(".etablissement-template .wpcf7-response-output").prependTo('#wpcf7-f3143-o1');
// $(".ets-edit-box .your-email input, .ets-edit-box .your-name input, .ets-edit-box .your-subject input").attr("disabled", true);

/*--------------------------------------------------------------
# PV
--------------------------------------------------------------*/

$(".page-template-template-list-col-12 .box-meta .btn-danger, .page-template-avantages-services .box-meta .btn-danger").addClass("mt-10");


/*--------------------------------------------------------------
# Validation carte
--------------------------------------------------------------*/

$(".validation-single .acf-form-submit").hide();
$(".waiting-for-validation").show();

if ( $( ".listener-validation select option:selected" ).text() == "Oui") {
		$(".acf-form-submit").show();
		$(".waiting-for-validation").hide();
	}

$(".listener-validation select").on('change', function(){    // 2nd (A)
	  if ( $( ".listener-validation select option:selected" ).text() == "Oui") {
		$(".acf-form-submit").toggle();
		$(".waiting-for-validation").toggle();

		} else if ( $( ".listener-validation select option:selected" ).text() == "Non") { 
		$(".acf-form-submit").toggle();
		$(".waiting-for-validation").toggle();

	}
});



/*--------------------------------------------------------------
# Loadmore
--------------------------------------------------------------*/


	


   // /* Masonry + Infinite Scroll */	
   //  var $container = $('#grid-container');
   //  $container.imagesLoaded(function () {
   //      $container.masonry({
   //          itemSelector: '.post'
   //      });
   //  });
   //  $('#grid-container').masonry({
   //      itemSelector: '.post',
   //      columnWidth: 258
   //  });
   //  $container.infinitescroll({
   //      navSelector: '#page-nav',
   //      nextSelector: '#page-nav a',
   //      itemSelector: '.post',
   //      loading: {
   //          msgText: 'Chargement des contenus...',
   //          finishedMsg: 'Aucun contenu à charger.',
   //          img: 'http://i.imgur.com/6RMhx.gif'
   //      }
   //  }, function (newElements) {
   //      var $newElems = $(newElements).css({
   //          opacity: 0
   //      });
   //      $newElems.imagesLoaded(function () {
   //          $newElems.animate({
   //              opacity: 1
   //          });
   //          $container.masonry('appended', $newElems, true);
   //      });
   //  });
   //  $(window).unbind('.infscr');
   //  jQuery("#page-nav a").click(function () {
   //      jQuery('#grid-container').infinitescroll('retrieve');
   //      return false;
   //  });
   //  $(document).ajaxError(function (e, xhr, opt) {
   //      if (xhr.status == 404) $('#page-nav a').remove();
   //  });   


/*--------------------------------------------------------------
# Responsive menu
--------------------------------------------------------------*/
   
$(".is-mobile").click(function(){
		$(".hd-menu").toggle();
		$("body").toggleClass('nav-need-help');
		$(".hidder2").toggle();
		$(".hd-menu").detach().appendTo('body');
	});


/*--------------------------------------------------------------
# Avantages/services #DEBUG!!!!!!!!!!!
--------------------------------------------------------------*/
   
$(".ct-avantages .is-Service").remove();
$(".ct-services .is-Avantage").remove();



/*--------------------------------------------------------------
# Page : Avatar hover accueil
--------------------------------------------------------------*/


$(".current-user-avatar img.avatar").on("mouseenter", function(){
		$("header .avatar-onhover").fadeToggle();
	});

$("header .avatar-onhover").on("mouseleave", function(){
		$("header .avatar-onhover").fadeToggle();
	});


/*--------------------------------------------------------------
# Page : Ajouter une offre d'emploi interne
--------------------------------------------------------------*/

$("#acf-field_5c779f631faf6, #acf-field_5c77a0881faf7, #acf-field_5c77a10c1fafa, #acf-field_5c77a13f1fafb").addClass('minimal');


/*--------------------------------------------------------------
# Single offre d'emploi
--------------------------------------------------------------*/


$(".btn-edit").on("click", function(){
		$(".content-offer").toggle();
		$(".offer-edit-box").toggle();

	});


$("#acf-field_5c779f631faf6, #acf-field_5c77a0881faf7, #acf-field_5c77a10c1fafa, #acf-field_5c77a13f1fafb").addClass('minimal');


/*--------------------------------------------------------------
# Modal : Offre d'emploi interne
--------------------------------------------------------------*/

$(" #ModalOE .alert-success").appendTo('.ct-offer .ct-title');

/*--------------------------------------------------------------
# Page : Mon compte
--------------------------------------------------------------*/

$("#wpua-thumbnail-existing, #wpua-preview-existing .description").remove();
// $("#wpua-preview-existing img").wrap('<div class="ct-avatar"></div>');

	$("#wppb_form_success_message").addClass('alert');
	$("#wppb_form_success_message").addClass('alert-success');
	$("#wppb_form_success_message").addClass('form-success-message');
	$("#wppb_form_success_message").removeAttr('id');
	$("#wpua-file-existing").addClass('btn');
	$("#wpua-file-existing").addClass('is-grey');

/*--------------------------------------------------------------
# Pagination
--------------------------------------------------------------*/

$(".pagination .next").remove();
$(".pagination .prev").remove();


/*--------------------------------------------------------------
# Formulaires
--------------------------------------------------------------*/ 

$(".wpcf7-submit").addClass("inner-btn");
// $(".wpcf7-mail-sent-ok").addClass("alert");
// $(".wpcf7-mail-sent-ok").addClass("alert-success");


/*--------------------------------------------------------------
# Page : Check card
--------------------------------------------------------------*/


// $(".single-card_dmo .acf-field-radio").appendTo(".ct-check-card .alert-warning");



/*--------------------------------------------------------------
# Page : Mon compte
--------------------------------------------------------------*/

$(".page-template-account .form-submit input").addClass("btn");
$(".page-template-account .form-submit input").addClass("padding10-20");
$("#wpua-upload-existing").text("Mettre à jour la photo");
$(".alert-danger.error p ").text("Le fichier choisi n'est pas conforme. Veuillez réessayer.");
$("#wpua-max-upload-existing").text("Taille maximale autorisée : 8192KB");
$("#wpua-upload-existing").addClass("search-submit");
$("#wpua-allowed-files-existing").text("Formats acceptés : jpg, jpeg, png, gif");
$("#wpua-upload-existing").addClass("padding10-20");
$(".wpua-edit .submit .button").remove();
$("#wpua-upload-button-existing").appendTo(".wpua-edit-container");




/*--------------------------------------------------------------
# Translate and styling tiled galery
--------------------------------------------------------------*/

$(".jp-carousel-commentlink").text("Cliquer pour commenter");
$(".jp-carousel-image-download").text("Voir en taille réelle");
$(".jp-carousel-comment-form-field").attr("placeholder","Écrivez ici votre commentaire ...");
$(".jp-carousel-comment-form-button").attr("value","Envoyer");

$(".jp-carousel-comment-post-error").text("Votre commentaire a l'air vide ... Veuillez saisir du texte.");
$(".jp-carousel-comment-post-error").css("color","#721c24");
$(".jp-carousel-comment-post-error").css("background-color","#f8d7da");
$(".jp-carousel-comment-post-error").css("border-color","#f5c6cb");

$(".jp-carousel-comment-post-success").text("Votre commentaire a été posté avec succès !");
$(".jp-carousel-comment-post-success").css("color","#155724");
$(".jp-carousel-comment-post-success").css("background-color","#d4edda");
$(".jp-carousel-comment-post-success").css("border-color","#c3e6cb");

$("#jp-carousel-commenting-as").remove();
$(".jp-carousel-comment-form-textarea").css("outline","none");
$(".jp-carousel-comment-form-textarea:focus").css("background-color","#fbfbfb!important");


$(".jp-carousel-comment-form-button").addClass("btn");
$(".jp-carousel-comment-form-button").css("background","#f8a460");
$(".jp-carousel-comment-form-button").css("border-radius","6px");

$(".jp-carousel-comment-post-success").css("font-family","'Raleway', sans-serif!important;");

/*--------------------------------------------------------------
# Page : Galerie
--------------------------------------------------------------*/

$(".acf-form-submit .acf-button").addClass("btn");
$(".acf-form-submit .acf-button").addClass("padding10-20");
$('.gallery').masonry({
    itemSelector: '.gallery-item', 
    isAnimated: true,
	  columnWidth: 40,
	  isFitWidth: true
});
$(".alm-reveal").addClass("row");

/*--------------------------------------------------------------
# Page : Revue de presse
--------------------------------------------------------------*/

$(".acf-form-submit .acf-button").addClass("btn");
$(".acf-form-submit .acf-button").addClass("padding10-20");
$('.gallery').masonry({
    itemSelector: '.gallery-item', 
    isAnimated: true,
	  columnWidth: 40,
	  isFitWidth: true
});




/*--------------------------------------------------------------
# Page : Formation
--------------------------------------------------------------*/

$( ".submit .button-primary" ).addClass( "padding10-20" );
$( "a.btn" ).addClass( "padding10-20" );
$(".page-template-formations p:contains('Powered by')").addClass("display-none");
$(".page-template-formations strong:contains('+')").addClass("display-none");
$( "<br>" ).insertAfter( ".alternate a:nth-child(2)" );
$('.em-warning.em-warning-confirms.notice.notice-success').addClass('alert');
$('.em-warning.em-warning-confirms.notice.notice-success').addClass('alert-success');
$('.statusnotice .notice').removeClass('em-warning');
$('.statusnotice .notice').removeClass('em-warning-confirms');
$('.statusnotice .notice').removeClass('notice-success');
$(".event-template-default p:contains('Vous avez déjà effectué une réservation')").addClass("alert");
$(".event-template-default p:contains('Vous avez déjà effectué une réservation')").addClass("alert-success");
$(".event-template-default p:contains('Vous avez déjà effectué une réservation') a").addClass("display-none");


/*--------------------------------------------------------------
# Page : Événements
--------------------------------------------------------------*/
$( ".post-type-archive-tribe_events #content" ).wrap( "<div class='ct-wrapper'></div>" );
$( ".post-type-archive-tribe_events #content" ).addClass( "ct-container" );
$( ".post-type-archive-tribe_events #content" ).addClass( "col-xl-10" );
$( ".post-type-archive-tribe_events #content" ).addClass( "offset-1" );
$( ".post-type-archive-tribe_events #content" ).removeClass( "row" );
$( ".tribe-events-button" ).addClass( "btn" );
$( ".ticket-cost" ).addClass( "btn" );
$( ".post-type-archive-tribe_events table a" ).addClass( "orange" );
$("#tribe-events-content-wrapper").find('h1').replaceWith(function() {
    return '<h2>' + $(this).text() + '</h2>';
});

$( ".single-tribe_events #primary" ).addClass( "ct-container" );
$( ".single-tribe_events #primary" ).addClass( "col-xl-10" );
$( ".single-tribe_events #primary" ).addClass( "offset-1" );
$( ".single-tribe_events #content" ).addClass( "col-xl-12" );
$( ".tribe-events-cost" ).addClass( "btn" );
$(".single-tribe_events .entry-content").find('h1').replaceWith(function() {
    return '<h2>' + $(this).text() + '</h2>';
});


/*--------------------------------------------------------------
# Page : Offres d'emplois
--------------------------------------------------------------*/
$(".single-offre_emploi .entry-content").find('h2').replaceWith(function() {
    return '<h3>' + $(this).text() + '</h3>';
});
$('#exampleModalLong').appendTo("body");



 $('#table-id')
    .on('click', 'th', function () {
      var index = $(this).index(),
          rows = [],
          thClass = $(this).hasClass('asc') ? 'desc' : 'asc';

      $('#table-id th').removeClass('asc desc');
      $(this).addClass(thClass);

      $('#table-id tbody tr').each(function (index, row) {
        rows.push($(row).detach());
      });

      rows.sort(function (a, b) {
        var aValue = $(a).find('td').eq(index).text(),
            bValue = $(b).find('td').eq(index).text();

        return aValue > bValue
             ? 1
             : aValue < bValue
             ? -1
             : 0;
      });

      if ($(this).hasClass('desc')) {
        rows.reverse();
      }

      $.each(rows, function (index, row) {
        $('#table-id tbody').append(row);
      });
    });

	$(".validation-radio").attr('disabled', true);
	$(".ct-downloads h1").addClass('h4', true);

/*--------------------------------------------------------------
# Rechercher un établissement
--------------------------------------------------------------*/

	$(".search-field").attr("placeholder", "Rechercher un établissement ...");
	
	$(".search .searchbar-ets .search-submit, .archive .searchbar-ets .search-submit").addClass("col-xl-2");
	$(".search .searchbar-ets .search-submit, .archive .searchbar-ets .search-submit").addClass("col-lg-2");

	$(".search .searchbar-ets label, .archive .searchbar-ets label").addClass("col-lg-10");
	$(".search .searchbar-ets label, .archive .searchbar-ets label").addClass("col-xl-10");

	$(".search .searchbar-ets label, .archive .searchbar-ets label").addClass("col-sm-10");
	$(".search .searchbar-ets .search-submit, .archive .searchbar-ets .search-submit").addClass("col-sm-2");

	$(".search .searchbar-ets .search-submit, .archive .searchbar-ets .search-submit").addClass("col-2");
	$(".search .searchbar-ets label, .archive .searchbar-ets label").addClass("col-10");


/*--------------------------------------------------------------
# Commentaires
--------------------------------------------------------------*/

$(".logged-in-as").remove();
$(".comment-form-comment label").remove();
$("#comments .form-submit .submit").addClass("btn");


	$(".acf-field-message").remove();
	$(".sidebar-btn, #sidebar-close").click(function(){
		$("body").toggleClass("sidebar-enabled");
		$("#secondary").animate({
			width: "toggle"
		})
		$(".hidder").fadeToggle();
		$(".close").fadeToggle();

	});

	$("#sidebar-trigger").click(function(){
		$("#sidebar-trigger").toggleClass("triggered");
		$("#sidebar-close").toggleClass("triggered");
		$("html").toggleClass("sidebar-enabled");
	});

	$("#sidebar-close").click(function(){
		$("#sidebar-trigger").toggleClass("triggered");
		$("#sidebar-close").toggleClass("triggered");
		$("html").toggleClass("sidebar-enabled");
	});

	$(".wcSubmitButton").attr("value", "Envoyer");
	$(".wcInput").attr("placeholder", "Entrez votre message ici");
	$("#user_login").attr("placeholder", "Exemple : jean.dupont@acsea.asso.fr");
	$("#user_pass").attr("placeholder", "Le mot de passe lié à votre adresse ACSEA");
	$("#wpua-add-button-existing").insertAfter("#wpua-images-existing");

	          getPagination('#table-id');
					//getPagination('.table-class');
					//getPagination('table');

		  /*					PAGINATION 
		  - on change max rows select options fade out all rows gt option value mx = 5
		  - append pagination list as per numbers of rows / max rows option (20row/5= 4pages )
		  - each pagination li on click -> fade out all tr gt max rows * li num and (5*pagenum 2 = 10 rows)
		  - fade out all tr lt max rows * li num - max rows ((5*pagenum 2 = 10) - 5)
		  - fade in all tr between (maxRows*PageNum) and (maxRows*pageNum)- MaxRows 
		  */
		 
	function getPagination (table){

        		var lastPage = 1 ; 

		  $('#maxRows').on('change',function(evt){
		  	//$('.paginationprev').html('');						// reset pagination 


		lastPage = 1 ; 
         $('.pagination').find("li").slice(1, -1).remove();
		  	var trnum = 0 ;									// reset tr counter 
		  	var maxRows = parseInt($(this).val());			// get Max Rows from select option

		  	if(maxRows == 5000 ){

		  		$('.pagination').hide();
		  	}else {
		  		
		  		$('.pagination').show();
		  	}

		  	var totalRows = $(table+' tbody tr').length;		// numbers of rows 
			 $(table+' tr:gt(0)').each(function(){			// each TR in  table and not the header
			 	trnum++;									// Start Counter 
			 	if (trnum > maxRows ){						// if tr number gt maxRows
			 		
			 		$(this).hide();							// fade it out 
			 	}if (trnum <= maxRows ){$(this).show();}// else fade in Important in case if it ..
			 });											//  was fade out to fade it in 
			 if (totalRows > maxRows){						// if tr total rows gt max rows option
			 	var pagenum = Math.ceil(totalRows/maxRows);	// ceil total(rows/maxrows) to get ..  
			 												//	numbers of pages 
			 	for (var i = 1; i <= pagenum ;){			// for each page append pagination li 
			 	$('.pagination #prev').before('<li data-page="'+i+'">\
								      <span>'+ i++ +'<span class="sr-only">(current)</span></span>\
								    </li>').show();
			 	}											// end for i 
			} 												// end if row count > max rows
			$('.pagination [data-page="1"]').addClass('active'); // add active class to the first li 
			$('.pagination li').on('click',function(evt){		// on click each page
				evt.stopImmediatePropagation();
				evt.preventDefault();
				var pageNum = $(this).attr('data-page');	// get it's number

				var maxRows = parseInt($('#maxRows').val());			// get Max Rows from select option

				if(pageNum == "prev" ){
					if(lastPage == 1 ){return;}
					pageNum  = --lastPage ; 
				}
				if(pageNum == "next" ){
					if(lastPage == ($('.pagination li').length -2) ){return;}
					pageNum  = ++lastPage ; 
				}

				lastPage = pageNum ;
				var trIndex = 0 ;							// reset tr counter
				$('.pagination li').removeClass('active');	// remove active class from all li 
				$('.pagination [data-page="'+lastPage+'"]').addClass('active');// add active class to the clicked 
				// $(this).addClass('active');					// add active class to the clicked 
				 $(table+' tr:gt(0)').each(function(){		// each tr in table not the header
				 	trIndex++;								// tr index counter 
				 	// if tr index gt maxRows*pageNum or lt maxRows*pageNum-maxRows fade if out
				 	if (trIndex > (maxRows*pageNum) || trIndex <= ((maxRows*pageNum)-maxRows)){
				 		$(this).hide();		
				 	}else {$(this).show();} 				//else fade in 
				 }); 										// end of for each tr in table
					});										// end of on click pagination list

		}).val(20).change();

												// end of on select change 
		 
    
  
								// END OF PAGINATION 
	}	

jQuery( document ).ready(function($) {
            $(".post-type-archive-tribe_events .ct-wrapper").addClass("col-xl-11");
            $(".post-type-archive-tribe_events .ct-wrapper").addClass("col-md-12");
            $(".post-type-archive-tribe_events #content").addClass("offset-xl-1");
            $(".post-type-archive-tribe_events #content").addClass("offset-md-0");
            $(".post-type-archive-tribe_events #content").removeClass("offset-1");
            $(".post-type-archive-tribe_events #content").removeClass("col-xl-10");
            $(".post-type-archive-tribe_events #content").removeClass("offset");
            $(".single-tribe_events #primary").addClass("col-xl-12");
            $(".single-tribe_events #primary").addClass("col-md-12");
            $(".single-tribe_events #primary").addClass("offset-md-0");
            $(".single-tribe_events #primary").removeClass("offset-1");
            $(".single-tribe_events #primary").removeClass("col-xl-10");
            $(".single-tribe_events #primary").removeClass("offset");
          });

});