<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 */

get_header();
$titre        =       get_field( "titre" );
$ets          =       get_field( "etablissement_ou_service" );
$description  =       get_field( "description" );
$photos       =       get_field( "vos_photos" );
$date         =       get_field("date_de_levenement");

?>

<div class="row col-xl-12">
    <div class="col-xl-3">
        <div class="ct-container col-xl-12 ct-inner-sidebar eq-padding ">
            <a href="../../galerie/add" class="btn padding10-20 no-mt">Ajouter un album</a>
        </div>
        <div class="ct-container col-xl-12">
            <h2 class="ct-title">Description de l'album</h2>
            <h3><?php the_title(); ?></h3>
            <?php echo $description; ?>
            <ul>
                <li><b>Établissement : </b><?php echo $ets ?></li>
                <li><b>Date de l'événement : </b><?php echo $date ?></li>
            </ul>
        </div>    
    </div>
    <div class="col-xl-9">
        <div class="ct-container ct-gallery col-xl-12">
            <div class="ct-title">
                <h2><?php the_title(); ?></h2>
                <?php include "breadcrumb.php"; ?> 
            </div>
            <br>
            <div class="row col-xl-12 justify-content-between">
      	        <div id="primary" class="content-area">
		                <main id="main" class="site-main">
		                    <?php echo $photos; ?>
		                </main>
	              </div>   
            </div>
        </div>  
    </div>
</div>
<?php
get_sidebar();
get_footer();
