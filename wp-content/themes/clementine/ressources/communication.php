<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package clementine
 * Template Name: Kit communication *
 
 */

get_header();

$terms = get_terms( 'ets', $args );
$date = get_the_date();
$time = get_the_time();
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_var = explode('/' , $actual_link);

          if ($url_var[6] == "communication") {
            $title = "Communication";
          } elseif ($url_var[6] == "handicap-autonomie-dependance") {
            $title = "Handicap Autonomie Dépendance";
          } elseif ($url_var[6] == "correspondance") {
            $title = "Correspondance";
          } 


// var_dump($terms);

?>

<div class="row col-xl-12">
    <div class="col-xl-3">
    <div class="ct-container col-xl-12 ct-inner-sidebar">
      <h2 class="ct-title">Ressources par type</h2>
          <?php echo do_shortcode('[subpages depth="-1" childof="1228"]'); ?>
    </div>    
  </div>
  <div class="col-xl-9">
    <div class="ct-container ct-docs col-xl-12">
      <div class="ct-title">
        <h2><?php echo $title ?></h2>
        <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
      </div>
      <div class="row col-xl-12 justify-content-between">
          <?php
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
          $args = array(

              'post_type' => 'ressources',
              'posts_per_page' => 5,
              'tax_query' => array(
                array(
                  'taxonomy' => 'type_ress',
                  'field'    => 'slug',
                  'terms'    => $url_var[6],
                ),
              ),
          );

          $query = new WP_Query( $args );
           if ( $query->have_posts() ) {
             
                while ( $query->have_posts() ) {
             
                    $query->the_post();
          $fichier = get_field ('field_5ceba9e5589ea');
          $description = get_field ('field_5cebaa4e589ec');
          $version = get_field('field_5cebaa12589eb');
          $ext = pathinfo($fichier, PATHINFO_EXTENSION);

 

          ?>
          <article id="post-<?php the_ID(); ?>" class="col-xl-12 col-lg-12">
            <div class="box-meta">
<!--               <img class="meta-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/files/<?php echo $ext ?>.png"> -->
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo the_post_thumbnail( 'medium' );   ?>
          </a> 
          <div class="meta-txt">
                <h3 class="meta-title">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php the_title(); ?>
                  </a> 
                </h3>
                <div class="meta-description"><?php echo $description ?></div>
                <div class="meta-date">
                  <span>Mis à jour le <?php echo the_modified_date( $d, $before, $after, $echo ); ?> à <?php echo the_modified_date('g:i a'); ?></span>
                </div>
                <a href="<?php echo $fichier ?>"><span class="meta-link btn padding10-20">Télécharger</span></a>
              </div>
            </div>
          </article>
      <?php
                }
             
            } else{ ?>
            <span class="alert alert-danger">Cette section est vide !</span>
              <?php
            }
              next_posts_link( 'Older Entries', $query->max_num_pages );
              previous_posts_link( 'Next Entries &raquo;' ); 
        wp_reset_postdata();
       
      ?>
  </div><!-- #primary -->
    </div>  
  </div> 

<?php
get_sidebar();
get_footer();
