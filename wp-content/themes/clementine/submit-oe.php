<?php /*Template Name: Offres d'emploi - Ajouter une offre */;?>
<?php acf_form_head(); ?>
<?php get_header(); ?>
<div id="container" class="col-xl-12 ask-form" >
	<div class="ct-container">
	    <div class="row">
	    	<div class="col-sm-12">
				<?php 
					while ( have_posts() ) : the_post(); 
				?>
				<div class="ct-title">
					<h2><?php the_title(); ?></h2>
   	     			<?php include "breadcrumb.php" ?>
				</div>
				<?php 
					the_content();
						$options = array(
						'post_id'					=> 'new_oe',
						'field_groups' 				=> array(2716),
						'post_title'				=> false,
						'post_type'					=> 'post',
						'post_status'				=> 'publish',
				        'return' 					=> '?my_param=true&updated=true',
						'submit_value'				=> 'Publier',
						'html_updated_message'		=> '<div class="message-ok alert alert-success" class="updated"><p>Offre d\'emploi publiée avec succès !</p></div>',
					);
				 	acf_form($options);
					endwhile; 
				?>
			</div>
		</div>
	</div>
</div>
<?php get_footer();