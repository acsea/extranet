<?php
/**
 * clementine functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package clementine
 */

if ( ! function_exists( 'clementine_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function clementine_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on clementine, use a find and replace
		 * to change 'clementine' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'clementine', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'clementine' ),
            'menu-rh' => esc_html__( 'Ressources humaines', 'clementine' ),
            'menu-cce' => esc_html__( 'CCE', 'clementine' ),
            'menu-syndicat' => esc_html__( 'syndicat', 'clementine' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'clementine_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'clementine_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function clementine_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'clementine_content_width', 640 );
}
add_action( 'after_setup_theme', 'clementine_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function clementine_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'clementine' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'clementine' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'clementine_widgets_init' );

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);

function my_jquery_enqueue() {
   wp_deregister_script('jquery');
    wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"), false);
    wp_register_script('jquery-ui', ("https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"), false);

    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui');

}

function bs_enqueue() {
    wp_enqueue_style('bootstrap4', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
    wp_enqueue_script( 'boot2','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array( 'jquery' ),'',true );
    wp_enqueue_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), '4.3.1', true );

}
add_action( 'wp_enqueue_scripts', 'bs_enqueue' );

/**
 * Enqueue scripts and styles.
 */
function clementine_scripts() {

	wp_enqueue_style( 'clementine-css', get_template_directory_uri() . '/assets/css/style.min.css', array(), '20190229', false );

	wp_enqueue_script( 'clementine-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), '20151215', true );

        wp_enqueue_script( 'jquery-masonry' );
    // wp_enqueue_script('infinite-scroll', get_template_directory_uri() .'/assets/js/infinite-scroll.pkgd.min.js', array('jquery'), null, true);
	wp_enqueue_script( 'clementine-skip-link-focus-fix', get_template_directory_uri() . '/assets/js/skip-link-focus-fix.js', array(), '20151215', true );
    wp_enqueue_script('search-js', get_template_directory_uri() .'/assets/js/search.js', array('jquery'), null, true);

	wp_enqueue_script('main-js', get_template_directory_uri() .'/assets/js/main-min.js', array('jquery'), null, true);



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'clementine_scripts' );

// add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
// function theme_enqueue_styles() {
//  wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

// }

function wpc_acf_google_map_api($api) {
	$api['key'] = 'AIzaSyCC81eCLhytw69ab2ZpWMF67PSev9l61tI';
	return $api;
}
add_filter('acf/fields/google_map/api', 'wpc_acf_google_map_api');


/*
 * Auto publish chat (general)
 */

function autopublish_chat( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_chat' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'publish',
        'post_title'    => $_POST['acf']['field_5da58ad115d91'],
        'post_type'  => 'channel',
        'tax_input' => array(
            'status' => array(127)
        )
  
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'autopublish_chat' );

/*
 * Auto publish chat (specific)
 */

function pending_chat_specific( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_chat_specific' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5da5809521d28'],
        'post_type'  => 'channel',
        'tax_input' => array(
            'status' => array(154)
        )
  
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'pending_chat_specific' );


/*
 * Pre save post DMO
 */

function my_pre_save_post_dmo( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_dmo' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        // 'post_title'    => 'test',
        'post_title'    => $_POST['acf']['field_5c4efddbb8d24'].' '.$_POST['acf']['field_5c4efdd6b8d23'].' - '.$_POST['acf']['field_5c4efdfeb8d27'],
        'post_type'  => 'card_dmo',
  
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_dmo' );




/*
 * Pre save post SATC
 */

function my_pre_save_post_satc( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_satc' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5ab409c12de'].' '.$_POST['acf']['field_5c5ab409c12ba'].' - '.$_POST['acf']['field_5c5ab409c133d'],
        'post_type'  => 'card_satc',
        
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_satc' );


/*
 * Pre save post DPS
 */

function my_pre_save_post_dps( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_dps' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5aeb5f0db8e'].' '.$_POST['acf']['field_5c5aeb5f0db56'].' - '.$_POST['acf']['field_5c5aeb5f0dc1b'],
        'post_type'  => 'card_dps',
        'submit_value'	=> 'Soumettre',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_dps' );

/*
 * Pre save post DIP
 */

function my_pre_save_post_dip( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_dip' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5aebe5bd17d'].' '.$_POST['acf']['field_5c5aebe5bd160'].' - '.$_POST['acf']['field_5c5aebe5bd1d2'],
        'post_type'  => 'card_dip',
        'submit_value'	=> 'Soumettre',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_dip' );

/*
 * Pre save post DEFI
 */

function my_pre_save_post_defi( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_defi' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5aec3a1bd38'].' '.$_POST['acf']['field_5c5aec3a1bd0c'].' - '.$_POST['acf']['field_5c5aec3a1bd90'],
        'post_type'  => 'card_defi',
        'submit_value'	=> 'Soumettre',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_defi' );


/*
 * Pre save post DAHAD
 */

function my_pre_save_post_dahad( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_dahad' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5aeca6bafb9'].' '.$_POST['acf']['field_5c5aeca6baf9a'].' - '.$_POST['acf']['field_5c5aeca6bb04a'],
        'post_type'  => 'card_dahad',
        'submit_value'	=> 'Soumettre',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_dahad' );


/*
 * Pre save post DAFHE
 */

function my_pre_save_post_dafhe( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_dafhe' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5aed9d26bbc'].' '.$_POST['acf']['field_5c5aed9d26b9e'].' - '.$_POST['acf']['field_5c5aed9d26c12'],
        'post_type'  => 'card_dafhe',
        'submit_value'	=> 'Soumettre',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_dafhe' );


/*
 * Pre save post ICB
 */

function my_pre_save_post_icb( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_icb' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5aee4c85e3c'].' '.$_POST['acf']['field_5c5aee4c85e1f'].' - '.$_POST['acf']['field_5c5aee4c85e93'],
        'post_type'  => 'card_icb',
        'submit_value'	=> 'Soumettre',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_icb' );


/*
 * Pre save post IME
 */

function my_pre_save_post_ime( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_ime' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5aeeaa2849d'].' '.$_POST['acf']['field_5c5aeeaa2847f'].' - '.$_POST['acf']['field_5c5aeeaa284f2'],
        'post_type'  => 'card_ime',
        'submit_value'	=> 'Soumettre',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_ime' );

/*
 * Pre save post Champ Goubert
 */

function my_pre_save_post_champ_goubert( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_champ_goubert' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5aef2f10fc3'].' '.$_POST['acf']['field_5c5aef2f10f95'].' - '.$_POST['acf']['field_5c5aef2f1101c'],
        'post_type'  => 'card_champ_goubert',
        'submit_value'	=> 'Soumettre',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_champ_goubert' );


/*
 * Pre save post IMPro SESSAD
 */

function my_pre_save_post_impro_sessad( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_impro_sessad' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c5aef970a901'].' '.$_POST['acf']['field_5c5aef970a8e2'].' - '.$_POST['acf']['field_5c5aef970a959'],
        'post_type'  => 'card_impro_sessad',
        'submit_value'	=> 'Soumettre',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_impro_sessad' );

/*
 * Pre save post gallery
 */

function my_pre_save_post_gallery( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_gallery' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5c99fb6cc56a9'],
        'post_type'  => 'gallery',
        
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_gallery' );


/*
 * Pre save post video
 */

function my_pre_save_post_video( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_video' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'pending',
        'post_title'    => $_POST['acf']['field_5d67e5f3c7e21'],
        'post_type'  => 'videos',
        
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'my_pre_save_post_video' );

/*
 * Front-end OE publishing
 */

function publish_post_oe( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_oe' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'publish',
        'post_title'    => $_POST['acf']['field_5cecf61e8ddfc'],
        'post_type'  => 'offre_emploi',
        'submit_value'  => 'Publier',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'publish_post_oe' );


/*
 * Front-end avantages services publishing
 */

function publish_post_as( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_as' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'publish',
        'post_title'    => $_POST['acf']['field_5d15dc43c144b'],
        'post_type'  => 'avantages_services',
        'submit_value'  => 'Publier',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'publish_post_as' );


/*
 * Front-end PV publishing
 */

function publish_post_pv( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_pv' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'publish',
        'post_title'    => $_POST['acf']['field_5d14da6ae30c3'],
        'post_type'  => 'pv',
        'submit_value'  => 'Publier',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'publish_post_pv' );


/*
 * Front-end newsletter publishing
 */

function publish_post_nl( $post_id ) {
 
    // check if this is to be a new post
    if( $post_id != 'new_nl' )
    {
        return $post_id;
    }
 
    // Create a new post
    $post = array(
        'post_status'  => 'publish',
        'post_title'    => $_POST['acf']['field_5d14d63c975be'],
        'post_type'  => 'newsletter_syndicat',
        'submit_value'  => 'Publier',
    );
 
    // insert the post
    $post_id = wp_insert_post( $post );
 
 
    // return the new ID
    return $post_id;
}
 
add_filter('acf/pre_save_post' , 'publish_post_nl' );



/*
 * Restrict Media Library acf_form 
 */

add_filter( 'posts_where', 'devplus_attachments_wpquery_where' );
function devplus_attachments_wpquery_where( $where ){
	global $current_user;

	if( is_user_logged_in() ){
		// we spreken over een ingelogde user
		if( isset( $_POST['action'] ) ){
			// library query
			if( $_POST['action'] == 'query-attachments' ){
				$where .= ' AND post_author='.$current_user->data->ID;
			}
		}
	}

	return $where;
}


/*
 * Add columns to exhibition post list
 */
 function add_acf_columns ( $columns ) {
   return array_merge ( $columns, array ( 
     'valide_par_la_direction' => __ ( 'Validé par la direction ?' ),
     'etablissement_ou_service'   => __ ( 'Établissement ou service' ) 
   ) );
 }
 add_filter ( 'manage_card_posts_columns', 'add_acf_columns' );


 /*
 * Add columns to exhibition post list
 */
 function card_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'valide_par_la_direction':
       echo get_post_meta ( $post_id, 'valide_par_la_direction', true );
       break;
     case 'etablissement_ou_service':
       echo get_post_meta ( $post_id, 'etablissement_ou_service', true );
       break;
   }
 }
 add_action ( 'manage_card_posts_custom_column', 'card_custom_column', 10, 2 );



/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


function cptui_register_my_cpts_card_dmo() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros dmo", "clementine" ),
		"singular_name" => __( "Carte pro dmo", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros dmo", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		'rewrite' => array( 'slug' => 'cartes-a-valider-dmo'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_dmo", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_dmo' );


function cptui_register_my_cpts_card_dip() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros dip", "clementine" ),
		"singular_name" => __( "Carte pro dip", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros dip", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-dip'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_dip", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_dip' );

function cptui_register_my_cpts_card_defi() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros defi", "clementine" ),
		"singular_name" => __( "Carte pro defi", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros defi", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-defi'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_defi", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_defi' );

function cptui_register_my_cpts_card_dafhe() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros dafhe", "clementine" ),
		"singular_name" => __( "Carte pro dafhe", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros dafhe", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-dafhe'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_dafhe", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_dafhe' );

function cptui_register_my_cpts_card_dahad() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros dahad", "clementine" ),
		"singular_name" => __( "Carte pro dahad", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros dahad", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-dahad'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_dahad", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_dahad' );

function cptui_register_my_cpts_card_dps() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros dps", "clementine" ),
		"singular_name" => __( "Carte pro dps", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros dps", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-dps'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_dps", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_dps' );

function cptui_register_my_cpts_card_satc() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros satc", "clementine" ),
		"singular_name" => __( "Carte pro satc", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros satc", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-satc'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_satc", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_satc' );

function cptui_register_my_cpts_card_icb() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros icb", "clementine" ),
		"singular_name" => __( "Carte pro icb", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros icb", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-icb'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_icb", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_icb' );


function cptui_register_my_cpts_card_champ_goubert() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros champ_goubert", "clementine" ),
		"singular_name" => __( "Carte pro champ_goubert", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros champ_goubert", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-champ-goubert'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_champ_goubert", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_champ_goubert' );

function cptui_register_my_cpts_card_IME() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros IME", "clementine" ),
		"singular_name" => __( "Carte pro IME", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros IME", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-ime'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_IME", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_IME' );

function cptui_register_my_cpts_card_impro_sessad() {

	/**
	 * Post Type: Cartes pros.
	 */

	$labels = array(
		"name" => __( "Cartes pros impro sessad", "clementine" ),
		"singular_name" => __( "Carte pro impro sessad", "clementine" ),
	);

	$args = array(
		"label" => __( "Cartes pros impro sessad", "clementine" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
        "hierarchical" => true,
        'rewrite' => array( 'slug' => 'cartes-a-valider-impro'),
		"query_var" => true,
		"menu_icon" => "dashicons-id-alt",
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "card_impro_sessad", $args );
}

add_action( 'init', 'cptui_register_my_cpts_card_impro_sessad' );

add_filter('default_content', 'gkp_default_editor_content');
function gkp_default_editor_content( $content ) {
    
    global $post_type;
    if ( $post_type == 'offre_emploi' ) {
        $content = "<p style=\"text-align: justify;\">Vous vous apprêtez à ajouter une nouvelle offre d'emploi. Voici quelques précautions d'usages :</p>

<ul>
 	<li style=\"text-align: justify;\">N'oubliez de remplir l'espace \"Paramètres de l'annonce\" situé en bas de cette page.</li>
 	<li style=\"text-align: justify;\">N'oubliez pas de renseigner le bloc \"Post expirator\" ci-contre pour que l'annonce soit retirée lorsqu'elle arrive à date buttoir (préférez l'option d'expiration \"Draft\" qui va mettre automatiquement votre annonce en mode \"Brouillon\" et qui ne va pas supprimer définitivement votre annonce, utile en cas d'erreur, vous pourrez récupérer cette annonce). Vous pourrez par la suite supprimer toutes les annonces en brouillon.</li>
 	<li style=\"text-align: justify;\">N'insérez aucun médias (images, etc ...).</li>
 	<li style=\"text-align: justify;\">Vous pouvez sauvegarder votre annonce et continuer sa rédaction plus tard sans qu'elle soit mise en ligne en cliquant sur \"Enregistrer le brouillon\" dans la boite ci-contre.</li>
 	<li style=\"text-align: justify;\">Si les boutons \"Publier\" ou \"Enregistrer le brouillon sont grisés, c'est que vous avez oublié de renseigner des informations obligatoires.</li>
 	<li style=\"text-align: justify;\">Veillez à harmoniser votre annonce avec celles qui sont déjà en ligne : pas d'utilisations injustifiées du gras, de l'italique, du soulignage... Si vous éprouvez des difficultés à effectuer une certaine action, enregistrez votre annonce en tant que brouillon et sollicitez le service communication afin d'obtenir de l'aide.</li>
 	<li style=\"text-align: justify;\">L'option \"Aperçu\" situé dans la boîte ci-contre vous permet de prévisualiser l'affichage d'une annonce sans la mettre en ligne.</li>
 	<li style=\"text-align: justify;\">N'hésitez pas à contacter l'administrateur du site internet en cas de soucis.</li>
 	<li style=\"text-align: justify;\">N'hésitez pas à vous référer au guide de modération de la gestion des offres d'emplois qui contient toute la procédure relative à la mise en ligne d'une annonce. Pour bénéficier de cette documentation, n'hésitez pas à solliciter le service communication.</li>
</ul>
<h2 style=\"text-align: justify;\"></h2>
<h2 style=\"text-align: justify;\">Missions principales</h2>
Renseignez les différentes missions principales ici.

&nbsp;
<h2 style=\"text-align: justify;\">Capacités requises</h2>
Renseignez les capacités requises ici.

&nbsp;
<h2 style=\"text-align: justify;\">Exigences - souhaits complémentaires</h2>
Renseignez les éventuels souhaits complémentaires ici.

&nbsp;
<h2 style=\"text-align: justify;\">Qualification</h2>
<p style=\"text-align: justify;\">Renseignez les qualifications ici.</p>
&nbsp;
<h2 style=\"text-align: justify;\">Contexte de l’emploi</h2>
<p style=\"text-align: justify;\">Renseignez le contexte de l'emploi ici.</p>
&nbsp;
";
        return $content;
    }

}

/* Disable WordPress Admin Bar for all users but admins. */
  show_admin_bar(false);

  add_image_size( 'info-acsea', 170, 241, true );


add_action( 'widgets_init', 'notes_widgets_init' );
add_filter( 'use_default_gallery_style', '__return_false' );

// message d'erreur si erreur de connexion
add_action( 'wp_login_failed', 'pippin_login_fail' );  // hook failed login
function pippin_login_fail( $username ) {
     $referrer = $_SERVER['HTTP_REFERER'];  // where did the post submission come from?
     // if there's a valid referrer, and it's not the default log-in screen
     if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
          wp_redirect(home_url() . '/connect/?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
          exit;
     }
}

// message d'erreur si connexion vide
add_action( 'wp_authenticate', '_catch_empty_user', 1, 2 );

function _catch_empty_user( $username, $pwd ) {
  if (empty($pwd)&&empty($username)) {
    wp_safe_redirect(home_url().'/connect/?login=empty');
    exit();
  }  if ( empty( $username )) {
    wp_safe_redirect(home_url() . '/connect/?user=empty' );
    exit();
  }
  if (empty($pwd)) {
    wp_safe_redirect(home_url().'/connect/?pwd=empty');
    exit();
  }
}

function disable_password_reset() { return false; }
add_filter ( 'allow_password_reset', 'disable_password_reset' );

add_filter("login_redirect", "gkp_subscriber_login_redirect", 10, 3);
function gkp_subscriber_login_redirect($redirect_to, $request, $user) {

  if(is_array($user->roles))
      if(in_array('administrator', $user->roles)) return site_url('/wp-admin/');

  return home_url();
}




/**
 * Redirect if guest
 *
 */

function redirect_guest() {
    if (is_user_logged_in()) {
        return 0;
    } else {
        wp_redirect( 'http://localhost:8888/extranet/connect/', 301 ); exit;
    }
}

/**
 * Redirect on first login
 *
 */

//hook when user registers
add_action( 'user_register', 'myplugin_registration_save', 10, 1 );

function myplugin_registration_save( $user_id ) {

    // insert meta that user not logged in first time
    update_user_meta($user_id, 'prefix_first_login', '1');

}

/**
 * Front-end delete offer
 *
 */
function delete_post(){
    global $post;
    $deletepostlink= add_query_arg( 'frontend', 'true', get_delete_post_link( get_the_ID() ) );
    if (current_user_can('edit_post', $post->ID)) {
        echo       '<a class="btn btn-danger meta-link padding10-20" onclick="return confirm(\'Êtes vous sûr de vouloir supprimer cette offre ?\')" href="'.$deletepostlink.'">Supprimer</a>';
    }
}

/**
 * Editer une offre d'emploi
 *
 */
function editer_post(){
        echo '<input class="btn btn-edit btn-secondary padding10-20" type="submit" value="Éditer l\'annonce" data-toggle="modal" data-target="#ModalOE">';
}

/**
 * Redirect after delete front-end post
 *
 */
add_action('trashed_post','trash_redirection_frontend');
function trash_redirection_frontend($post_id) {
    if ( filter_input( INPUT_GET, 'frontend', FILTER_VALIDATE_BOOLEAN ) ) {
        wp_redirect( get_option('siteurl').'/ressources-humaines/offres-demplois-internes/' );
        exit;
    }
}


/**
 * Logout without confirmation
 *
 */

add_action('check_admin_referer', 'logout_without_confirm', 10, 2);
function logout_without_confirm($action, $result)
{
    /**
     * Allow logout without confirmation
     */
    if ($action == "log-out" && !isset($_GET['_wpnonce'])) {
        $redirect_to = isset($_REQUEST['redirect_to']) ? $_REQUEST['redirect_to'] : '';
        $location = str_replace('&amp;', '&', wp_logout_url($redirect_to));;
        header("Location: $location");
        die;
    }
}


// expire offer posts on date field.
if (!wp_next_scheduled('expire_posts')){
  wp_schedule_event(time(), 'twicedaily', 'expire_posts'); // this can be hourly, twicedaily, or daily
}

add_action('expire_posts', 'expire_posts_function');

function expire_posts_function() {
    $today = date('Ymd');
    $args = array(
        'post_type'  => 'offre_emploi',
        'posts_per_page' => -1 
    );
    $posts = get_posts($args);
    foreach($posts as $p){
        $expiredate = get_field('field_5c77a0ed1faf9', $p->ID, false, false); // get the raw date from the db
        if ($expiredate) {
            if($expiredate < $today){
                $postdata = array(
                    'ID' => $p->ID,
                    'post_status' => 'draft'
                );
                wp_update_post($postdata);
            }
        }
    }
}

/**
 * Track user amount of login
 *
 */
add_action( 'wp_login', 'track_user_logins', 10, 2 );
function track_user_logins( $user_login, $user ){
    if( $login_amount = get_user_meta( $user->id, 'login_amount', true ) ){
        // They've Logged In Before, increment existing total by 1
        update_user_meta( $user->id, 'login_amount', ++$login_amount );
    } else {
        // First Login, set it to 1
        update_user_meta( $user->id, 'login_amount', 1 );
    }
}


/**
 *
 * Menu en fonction des rôles sur l'index
 *
 */
function my_shortcuts(){
    global $current_user; 
    wp_get_current_user();
    get_currentuserinfo();
    $roles=$current_user->roles;    
    if (in_array("ressources-humaines", $roles)){
          wp_nav_menu( array(
            'theme_location' => 'menu-rh',
            'menu_id'        => 'menu-rh',
        ) );
    } elseif (in_array("cce", $roles)) {
          wp_nav_menu( array(
            'theme_location' => 'menu-cce',
            'menu_id'        => 'menu-cce',
        ) );
    } elseif (in_array("syndicat", $roles)) {
          wp_nav_menu( array(
            'theme_location' => 'menu-syndicat',
            'menu_id'        => 'menu-syndicat',
        ) );
    } else{
        return 0;
    }
}
function display_my_shortcuts(){
    global $current_user; 
    wp_get_current_user();
    get_currentuserinfo();
    $roles=$current_user->roles;
    if (in_array("ressources-humaines", $roles) || in_array("cce", $roles) || in_array("syndicat", $roles)) { 
        echo "<div class='col-xl-12 col-lg-12'>
        <div class='ct-raccourcis ct-container ct-container-xs col-xl-12'>
        <h2 class='ct-title'>Mes raccourcis</h2>";
        my_shortcuts();
        echo"</div>
        </div>"; }
}




/**
 *
 * Remplir les champs noms et prénoms de l'user avec les éléments de son adresse mail à sa première connexion
 *
 */
add_action( 'user_register', 'fill_identity', 10, 1 );
function fill_identity($user_id){

    $current_user = get_userdata($user_id);
    $mail_user = $current_user->user_email;
    $mail_user = substr($mail_user,0,-14);
    $mail_user = explode(".", $mail_user);
    $prenom = $mail_user[0] = ucfirst($mail_user[0]);
    $nom = $mail_user[1] = ucfirst($mail_user[1]);
    var_dump($mail_user);
    wp_update_user(array(
        'ID' => $current_user->ID, 
        'first_name' => $prenom,
        'last_name' => $nom,
    ));
}

add_action( 'init', 'blockusers_init' );



/**
 *
 * Bloquer l'accès au WP admin pour les users
 *
 */
function blockusers_init() {
    if ( is_admin() && ! current_user_can( 'administrator' ) && 
       ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
        wp_redirect( home_url() );
        exit;
    }
}


/**
 *
 * Load more for Info ACSEA
 *
 */
function load_posts_by_ajax_callback() {
    check_ajax_referer('load_more_posts', 'security');
    $paged = $_POST['page'];
    global $wp;
    $current_slug = add_query_arg( array(), $wp->request );
    $args = array(
        'post_type' => 'info_acsea',
        'post_status' => 'publish',
        'posts_per_page' => '8',
        'paged' => $paged,
    );
    $my_posts = new WP_Query( $args );
    // var_dump($_SERVER['REQUEST_URI']);
    if ( $my_posts->have_posts() ) :
        ?>
        <?php while ( $my_posts->have_posts() ) : $my_posts->the_post(); ?>
            <!-- <div><?php var_dump($current_slug) ?></div> -->
        <article id="post-<?php the_ID(); ?>" class="infoacsea-article item-single col-xl-3 col-sm-4 col-12">
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo the_post_thumbnail( 'medium' );   ?>
          </a> 
          <div class="box-meta">
            <h3>
              <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_title(); ?>
              </a> 
            </h3>
            <?php the_excerpt(); ?>           
          </div>
        </article>

        <?php endwhile; ?>
        <?php
    endif;
 
    wp_die();
}
add_action('wp_ajax_load_posts_by_ajax', 'load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'load_posts_by_ajax_callback');



/**
 *
 * Load more button
 *
 */

function loadmore(){
    echo "<div class='loadmore btn width-100 padding10-20'>Charger plus ...</div>";
}


function custom_comments( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' : ?>
            <li <?php comment_class(); ?> id="comment<?php comment_ID(); ?>">
            <div class="back-link">< ?php comment_author_link(); ?></div>
        <?php break;
        default : ?>
            <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
                <?php echo get_avatar( $comment, 100 ); ?>
                <article <?php comment_class(); ?> class="comment comment-single">
                    <div class="comment-body">
                        <div class="author vcard">
                            <span class="author-name">
                                <?php comment_author(); ?>        
                            </span>
                            <footer class="comment-footer">
                                <time <?php comment_time( 'c' ); ?> class="comment-time">
                                    <span class="date">
                                        <?php comment_date(); ?>
                                    </span>
                                    <span class="time">
                                        <?php comment_time(); ?>
                                    </span>
                                </time>
                            </footer>
                            <?php comment_text(); ?>
                        </div>
                    </div>
 

            <div class="reply"><?php 
            comment_reply_link( array_merge( $args, array( 
            'reply_text' => 'Reply',
            'after' => ' <span>&amp;amp;darr;</span>', 
            'depth' => $depth,
            'max_depth' => $args['max_depth'] 
            ) ) ); ?>
            </div><!-- .reply -->

 
            </article><!-- #comment-<?php comment_ID(); ?> -->
        <?php // End the default styling of comment
        break;
    endswitch;
}