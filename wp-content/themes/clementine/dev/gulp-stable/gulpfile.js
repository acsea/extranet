var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require("gulp-rename");
var strip = require('gulp-strip-comments');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;
var minify = require('gulp-minify');

// Compile Sass to CSS


// Concat all JS files
// gulp.task('js', function () {
//   gulp.src([
//     '../js/src/skip-link-focus-fix.js',
//     // '../js/src/navigation.js',
//     '../js/src/main.js'
//   ])
//   // .pipe(jshint())
//   // .pipe(jshint.reporter(stylish))
//   .pipe(uglify().on('error', function(e){
//       console.log(e);
//   }))
//   .pipe(concat('main-dist.js'))
//   .pipe(gulp.dest('../js/dist'));
// });


// gulp.task('browser-sync', function() {
//     browserSync.init({
//         proxy: "http://localhost:8888/extranet/"
//     });
// });

gulp.task('sass', function () {
  gulp.src('../../assets/sass/**/*.scss')
  .pipe(sass({outputStyle: 'nested'}).on('error', sass.logError))
  .pipe(autoprefixer({ browsers: ['last 5 versions', 'ie 8-11'], cascade: false }))
  .pipe(cleanCSS({compatibility: 'ie8'}))
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest('../../assets/css/'))
  .pipe(browserSync.stream());
});


 
gulp.task('javascript', function() {
  gulp.src('../../assets/js/main.js')
    .pipe(minify())
    .pipe(gulp.dest('../../assets/js/'))
});




// gulp.task('watch', ['browser-sync','sass'], function () {
//   gulp.watch('../../assets/sass/**/*.scss', ['sass']);
// });

gulp.task('watch', ['sass', 'javascript'], function () {
  gulp.watch('../../assets/sass/**/*.scss', ['sass']);
  gulp.watch('../../assets/js/*.js', ['javascript']);

});

// Browser Sync + Watching scss/php files
// gulp.task('serve', ['sass'], function() {
    // browserSync.init({proxy: "http://192.168.0.30/newman"});
    // livereload({start: true, port: 8090});
    // gulp.watch('../sass/**/*.scss', ['sass']);
    // gulp.watch('../js/src/**/*.js', ['js']).on('change', browserSync.reload);
    // gulp.watch('../**/*.php').on('change', browserSync.reload);
// });

// Default Gulp Task
gulp.task('default', ['sass', 'javascript', 'watch']);
