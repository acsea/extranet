<?php
/**
 * Page : Revues de presse
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 * 
 */

get_header();
$categories       =       get_the_category();
$category_name    =       $categories[0]->name;

?>

<div class="col-xl-12">
    <div class="row ct-rdp justify-content-md-center">
        <?php include "breadcrumb.php"; ?>
        <div class="col-xl-3 d-flex flex-column">
            <div class="meta-info">
                <h2><?php echo $category_name ?></h2>
                <p>La revue de presse contient les différents articles parus dans les journaux concernant les établissements et l'activité de l'association.</p>
            </div>
        </div>
        <div class="ct-container ct-featured row col-xl-9">
            <?php echo do_shortcode('[ajax_load_more id="2895547398" container_type="div" repeater="template_5" post_type="post" posts_per_page="8" transition_container_classes="row col-xl-12" category="revues-de-presse"]'); ?>
        </div>
    </div>
</div>
<?php
get_sidebar();
get_footer();
