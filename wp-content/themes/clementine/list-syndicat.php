<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package clementine
 * Template Name: Syndicat - liste
 
 */

get_header();

$terms = get_terms( 'ets', $args );
$date = get_the_date();
$time = get_the_time();
$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url_var = explode('/' , $actual_link);

?>

<div class="row col-xl-12">
    <div class="col-xl-3">
              <?php 
                    $roles=$current_user->roles;
                    if (in_array("syndicat", $roles) or (in_array("administrator", $roles))){  ?>
    <div class="ct-container col-xl-12 ct-inner-sidebar eq-padding">

                      <a href="add/" class="btn width-100 padding10-20">Ajouter une newsletter</a>                    

    </div>
              <?php 
                    }
              ?>
    <div class="ct-container col-xl-12 ct-inner-sidebar">
      <h2 class="ct-title">Syndicats</h2>
          <?php echo do_shortcode('[subpages depth="-1" childof="3075"]'); ?>
    </div>    
  </div>
  <div class="col-xl-9">
    <div class="ct-container ct-syndicat syndicat-<?php echo $url_var[5] ?> col-xl-12">
      <div class="ct-title">
        <h2><?php the_title(); ?></h2>
        <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
      </div>
      <div class="row col-xl-12 justify-content-between">
          <?php
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
          $args = array(

              'post_type' => 'newsletter_syndicat',
              'posts_per_page' => 15,
              // 'tax_query' => array(
              //   array(
              //     'taxonomy' => 'service',
              //     'field'    => 'slug',
              //     'terms'    => $url_var[5],
              //   ),
              // ),

          );

          $query = new WP_Query( $args );
           if ( $query->have_posts() ) {
             
                while ( $query->have_posts() ) {
             
                    $query->the_post();
                    $syndicat=get_field("field_5d482c9dd77de");
                    $link=get_field("field_5d14d646975bf");
                    $description=get_field("field_5d14d659975c0");
                    $date=get_field("field_5d14d6a609f86");
                    $title=get_field("field_5d14d63c975be");




 

          ?>
          <article id="post-<?php the_ID(); ?>" class="col-lg-6 col-xl-6 <?php echo $syndicat ?>">
            <div class="box-meta ">
<!--               <img class="meta-img" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/files/<?php echo $ext ?>.png"> -->
          <div class="meta-txt d-flex align-items-center justify-content-center flex-column">
                <h3 class="meta-title">
                  <a href="<?php echo $link ?>" title="<?php the_title_attribute(); ?>" target="_blank">
                    <?php the_title(); ?>
                  </a> 
                </h3>
<!--                 <div class="meta-description">
                    <?php echo wp_trim_words( $content , '20' ); ?>
                </div> -->
                <div class="meta-date">
                  <span><?php echo $date ?></span>
                </div>
                <a href="<?php echo $link ?>" title="<?php the_title_attribute(); ?>" target="_blank"><span class="meta-link btn padding10-20">Lire</span></a>
              <?php 
                    $roles=$current_user->roles;
                    if (in_array("syndicat", $roles) or (in_array("administrator", $roles))){  
                      echo delete_post(); 
                    }
              ?>
              </div>
            </div>
          </article>
      <?php
                }
             
            } else{ ?>
            <span class="alert alert-danger">Cette section est vide !</span>
              <?php
            }
              next_posts_link( 'Older Entries', $query->max_num_pages );
              previous_posts_link( 'Next Entries &raquo;' ); 
        wp_reset_postdata();
       
      ?>
  </div><!-- #primary -->
    </div>  
  </div> 

<?php
get_sidebar();
get_footer();
