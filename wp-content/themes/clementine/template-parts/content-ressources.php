<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package clementine
 */
get_header();
$terms        =     get_terms( 'ets', $args );
$term         =     get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
$term_name    =     $term->name;
$term_slug    =     $term->slug;
$cpt          =     get_post_type( $post->ID );
$tax_name     =     get_query_var( 'taxonomy' );


  
if ($type == "URL") { $url = $link; $button="Aller au site"; } else { $url = $fichier; $button="Télécharger";}
?>

<div class="row col-xl-12">
    <div class="col-xl-3">
    <div class="ct-container col-xl-12 ct-inner-sidebar">
      <h2 class="ct-title">Trier par type</h2>
        <?php 
            $args     =     array('hide_empty' => false);
            $terms    =     get_terms( $tax_name, $args );
                foreach ( $terms as $term ) {
                    // The $term is an object, so we don't need to specify the $taxonomy.
                    $term_link = get_term_link( $term );
                    // If there was an error, continue to the next term.
                    if ( is_wp_error( $term_link ) ) { continue; }
                    // We successfully got a link. Print it out.
                    echo '<div><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a>';
                    echo  term_description($post->ID,$term).'</div>';
                }
        ?>
    </div>
      <?php if ($cpt == "ressources") { ?>
      <div class="ct-container col-xl-12 ct-inner-sidebar">
        <h2 class="ct-title">Trier par format</h2>
        <?php 
            $args     =     array('hide_empty' => false);
            $terms    =     get_terms( 'format', $args );
                foreach ( $terms as $term ) {
                    // The $term is an object, so we don't need to specify the $taxonomy.
                    $term_link = get_term_link( $term );
                    // If there was an error, continue to the next term.
                    if ( is_wp_error( $term_link ) ) { continue; }
                    // We successfully got a link. Print it out.
                    echo '<div><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a>';
                    echo  term_description($post->ID,$term).'</div>';
                }
        ?>
      </div>
      <?php } ?>    
  </div>
  <div class="col-xl-9">
    <div class="ct-container ct-docs col-xl-12">
      <div class="ct-title">
        <h2><?php echo $term_name ?></h2>
        <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
      </div>
      <div class="row col-xl-12 justify-content-between">

            <?php
              $paged    =     ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
              $args     =     array('post_type' => $cpt,     'tax_query' => array(
        array(
            'taxonomy' => $tax_name,
            'field'    => 'slug',
            'terms'    => $term_slug,
        ), ),
              'posts_per_page' => 10, 'paged' => $paged);
              $query    =     new WP_Query( $args );
              if ( $query->have_posts() ) { ?>

            <?php while ( $query->have_posts() ) :
                $query      ->    the_post();
                $fichier      =     get_field ('field_5ceba9e5589ea');
                $description    =     get_field ('field_5cebaa4e589ec');
                $version      =     get_field('field_5cebaa12589eb');
                $ext        =     pathinfo($fichier, PATHINFO_EXTENSION);
                $type       =     get_field('field_5daf01fdf8a5e');
                $link       =     get_field('field_5daf022cf8a5f');
                $button       =     "";
                $url        =     "";
          
                if ($type == "URL") { $url = $link; $button="Aller au site"; } else { $url = $fichier; $button="Télécharger";}
        ?>

          <article id="post-<?php the_ID(); ?>" class="col-xl-12 col-lg-12">
            <div class="box-meta">
              <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php echo the_post_thumbnail( 'medium' );   ?>
          </a> 
              <div class="meta-txt">
                <h3 class="meta-title">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <?php the_title(); ?>
                  </a> 
                </h3>
                <div class="meta-description"><?php echo $description ?></div>
                <div class="meta-date">
                  <span>Mis à jour le <?php echo the_modified_date( $d, $before, $after, $echo ); ?> à <?php echo the_modified_date('g:i a'); ?></span>
                </div>
                <a href="<?php echo $fichier ?>"><span class="meta-link btn padding10-20">Télécharger</span></a>
              </div>
            </div>
          </article>
        <?php endwhile; ?>

        <?php include 'pagination.php'; ?>
      <?php   
                }
            else { 
            ?>
              <span class="alert alert-danger">Cette section est vide !</span>
            <?php
              }
              wp_reset_postdata();
          ?>
      </div><!-- #primary -->
    </div>  
  </div> 
<?php
get_sidebar();
get_footer();
