<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 */

/* Template Name: Submit articles */


get_header();
?>

<div class="row col-xl-12">
  <div class="col-xl-3">
    <div class="ct-container ct-shortcuts col-xl-12">
      <h2 class="ct-title">Raccourcis</h2>
      <?php echo do_shortcode('[subpages depth="-1" childof="1237"]'); ?>
    </div>    
  </div>
  <div class="col-xl-9">
    <div class="ct-container ct-gallery col-xl-12">
      <div class="ct-title">
        <h2><?php the_title(); ?></h2>
      </div>
      <div class="row col-xl-12 justify-content-between">
      	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->   
      </div>
    </div>  
  </div>

</div>

<?php
get_sidebar();
get_footer();
