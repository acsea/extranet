<?php 
/*Template Name: Médias - Ajouter une vidéo*/
acf_form_head(); 
get_header(); 
?>
<div class="row col-xl-12">
  	<div class="col-xl-12">
    	<div class="ct-container ct-gallery col-xl-12">
      		<div class="ct-title">
        		<h2><?php the_title(); ?></h2>
         		<?php include "breadcrumb.php"; ?> 
      		</div>
      		<div class="row col-xl-12 justify-content-between">
      			<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<?php 
							while ( have_posts() ) : the_post(); 
								the_content();
								$options = array(
									'post_id'				=> 			'new_video',
									'field_groups' 			=> 			array(3808),
									'post_title'			=> 			false,
									'post_type'				=> 			'videos',
									'post_status'			=> 			'draft',
							        'return' 				=> 			'?my_param=true&updated=true',
									'submit_value'			=> 			'Envoyer ma demande',
									'html_updated_message'	=> 			'<div class="alert alert-success message-ok" class="updated"><p>Votre demande a été traité avec succès ! Elle est désormais soumise à validation auprès de la direction. Une fois validée, la vidéo sera disponible sur la page <a href="../">Vidéos</a>.</p></div>',
								);
								acf_form($options);
							endwhile; 
						?>
					</main>
				</div>
      		</div>
    	</div>  
  	</div>
</div>
<?php get_footer(); 