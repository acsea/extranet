<?php 
/* Template Name: Établissements */
get_header(); 
acf_form_head();
get_currentuserinfo();
global $current_user;
$adress 		= 		get_field( "adresse" );
$adress2 		= 		get_field( "adresse_2" );
$zipcode 		= 		get_field( "code_postal" );
$city 			= 		get_field( "ville" );
$telephone1 	= 		get_field( "numero_de_telephone_1" );
$telephone2 	= 		get_field( "numero_de_telephone_2" );
$fax 			= 		get_field( "numero_de_fax" );
$mail 			= 		get_field( "adresse_mail" );
$gmaps 			= 		get_field( "gmaps" );
$pole 			= 		get_field_object('field_5c92457756909');
$pole2 			= 		$pole['value'][0];
$mail_user		=		$current_user->user_email;
$name_user		=		$current_user->user_firstname." ".$current_user->user_lastname;
?>
<div class="row col-xl-12 ct-ets">
  	<div class="col-xl-3">
    	<div class="ct-container col-xl-12 ct-meta">
			<div class="ct-img">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="ets">
			</div>
			<div class="ct-info res-padding30-35">
				<h2><?php the_title(); ?></h2><br>
				<p><a class="<?php echo $pole2->slug; ?>" href="../../pole/<?php echo $pole2->slug; ?>">Pôle <?php echo $pole2->name; ?></a></p><br>
				<?php echo $adress; ?><br>
				<?php echo $zipcode; ?> <?php echo $city; ?><br><br>
				<?php echo $telephone1; ?><br>
				<?php echo $telephone2; ?><br>
				<?php echo $fax; ?><br>
				<a href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a><br>
				<a href="https://www.google.com/maps/dir/?api=1&destination=<?php echo $adress ?>, <?php echo $zipcode ?>, <?php echo $city ?>">Ouvrir dans Google Maps</a>
	      		<a class="btn padding10-20 width-100" id="submitChange" href="#">Signaler un changement</a>
			</div>
    	</div>    
  	</div>
  	<div class="col-xl-9">
    	<div class="ct-container col-xl-12 no-padding ct-mapzone">
      		<div class="row col-xl-12 justify-content-between no-padding">
      			<div id="primary" class="content-area">
					<main id="main" class="site-main">
						<?php include "breadcrumb.php"; ?>
						<div class="acf-map">
							<div class="marker" data-lat="<?php echo $gmaps['lat']; ?>" data-lng="<?php echo $gmaps['lng']; ?>"></div>
						</div>
						<div class="ets-edit-box ct-container col-xl-12">
                    		<h2>Signaler un changement</h2>
        					<?php echo do_shortcode( '[contact-form-7 id="3143" title="Signaler un changement"]' ); ?>
      					</div>
					</main>
				</div>   
      		</div>
    	</div>  
  	</div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCC81eCLhytw69ab2ZpWMF67PSev9l61tI"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/map.js"></script>
<?php
get_sidebar();
get_footer();