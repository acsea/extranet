<?php
/*
Template Name: Login page
*/
get_header('login'); ?>
<div class="container h-100">
	<div class="row h-100 justify-content-center align-items-center">
		<div class="ct-container ct-login col-12 row">
			<div class="col-xl-4 ct-disclaimer">
				<img class="login-logo" src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logo.png" alt="Logo" />
				<h1>Bienvenue sur l'extranet ACSEA</h1>
				<p>Pour vous connecter, veuillez utiliser les identifiants liés à votre adresse mail.</p>
					<ul>
						<li><a href="http://www.acsea.asso.fr">Retour sur le site ACSEA</a></li>
						<li><a href="#" id="reinitPassword">Mot de passe oublié ?</a></li>
						<li><a href="mailto:communication@acsea.asso.fr" id="reinitPassword">Demander une assistance technique</a></li>
					</ul>
			</div>
			<div class="col-xl-8 ct-form-connect">
			<?php
				$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
				if (strpos($url, 'connect/?user=empty')!==false) {
					echo "<div class='login_failed animated shake alert alert-danger' role='alert'>Merci de renseigner votre adresse mail pour vous connecter</div>";
				}
				if (strpos($url, 'connect/?pwd=empty')!==false) {
					echo "<div class='login_failed alert animated shake alert-danger' role='alert'>Merci de renseigner votre mot de passe pour vous connecter/div>";
				}
				if (strpos($url, 'connect/?login=empty')!==false) {
					echo "<div class='login_failed alert animated shake alert-danger' role='alert'>Merci de renseigner votre adresse mail et votre mot de passe pour vous connecter</div>";
				}
				if (strpos($url,'connect/?login=failed') !== false) {
					echo "<div class='login_failed alert alert-danger animated shake' role='alert'>Le mot de passe ou l'adresse mail est incorrect.</div>";
				}	
			?>
				<div id="connectForm">
					<form method="post" action="../wp-login.php" id="loginform" name="loginform">
					  	<p class="login-username">
					    	<label for="user_login">Identifiant</label>
					    	<input type="text" size="20" value="" id="user_login" name="log">
					  	</p>
					  	<p class="login-password">
					    	<label for="user_pass">Mot de passe</label>
					    	<input type="password" tabindex="20" size="20" value="" id="user_pass" name="pwd">
					  	</p>
					  	<p class="login-remember">
					  		<label class="stayconnected">
					  			<input type="checkbox" tabindex="90" value="forever" id="rememberme" class="rememberme" name="rememberme">Rester connecté
					  		</label>
					  	</p>
					  	<p class="login-submit">
					    	<input type="submit" tabindex="100" value="Se connecter" id="wp-submit" class="btn" name="wp-submit">
					    	<input type="hidden" value="http://www.VOTRE_SITE.com/" name="redirect_to">
					  	</p>
					</form>					
				</div>
				<div id="connectReinit">
					<?php echo do_shortcode("[wppb-recover-password]"); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer('login'); ?>