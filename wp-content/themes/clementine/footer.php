<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package clementine
 */

?>

        <footer id="colophon" class="site-footer container-fluid">
            <?php wp_footer(); ?>
        </footer><!-- #colophon -->
    </div><!-- #page -->

        <!-- Modal pour postuler à une offre d'emploi -->
        <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="exampleModalLongTitle">Postuler à l'annonce</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php echo do_shortcode('[contact-form-7 id="120" title="Formulaire de candidature suite à une annonce"]'); ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal pour contact - assistance -->
        <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModalTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title" id="helpModalTitle">Nous contacter</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php echo do_shortcode('[contact-form-7 id="3618" title="Nous contacter"]'); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php 
            if ($nb_login==1) { include "welcome.php"; }
            $ets    =     get_field( "field_5c779f631faf6" ); 
        ?>

        <script type="text/javascript">
            var title   =     "<?php the_title(); ?>";
            var etab    =     "<?php echo $ets; ?>";
            document.getElementById('title-oe').value =   title;
           	document.getElementById('etsName').value =    etab;
            
        </script>
    </body>
</html>
