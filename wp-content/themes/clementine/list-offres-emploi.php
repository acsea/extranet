<?php 
/* Template Name: Offres d'emplois - Liste */
get_header();
?>
<div class="row col-xl-12">
    <div class="col-xl-12">
        <div class="ct-container ct-container-xs ct-offer col-xl-12">
            <?php include "template-parts/header_page.php"; ?>
            <div class="alm-container"></div>
            <?php echo do_shortcode('[ajax_load_more id="9979175219" container_type="div" css_classes="alm-container" repeater="template_8" post_type="offre_emploi"]'); ?>
        </div>
        <!--<span class="alert alert-danger">Cette section est vide !</span>-->          
        <?php 
            get_footer(); 
        ?>
    </div>
</div>
<?php get_sidebar(); ?>