<?php 
/* Template Name: Home */
get_header();
?>
	<div class="col-12 row">
		<div class="col-xl-6 col-lg-12">
			<div class="col-xl-12 col-lg-12">
				<div class="ct-access ct-container ct-container-xs col-xl-12">
					<h2 class="ct-title">Mes accès rapides</h2>
					<?php redirect_guest(); ?>
					<div class="row">
						<a class="col-xl-6" href="personnel/annuaire" target="_blank">
							<div>
								<div class="ct-box col-xl-12 box box-cce">
									<h3 class="box-title">Comité Centrale d'Entreprise</h3>
								</div>
							</div>
						</a> 
						<a class="col-xl-6" href="https://webtime.acsea.asso.fr/WD180AWP/WD180AWP.EXE/CONNECT/wEoctime80" target="_blank">
							<div>
								<div class="ct-box col-xl-12 box box-octime">
									<h3 class="box-title">Octime</h3>
								</div>
							</div>
						</a> 
						<a class="col-xl-6" href="personnel/annuaire" target="_blank">
							<div>
								<div class="ct-box col-xl-12 box box-annuaire">
									<h3 class="box-title">Annuaire</h3>
								</div>
							</div>
						</a> 
						<a class="col-xl-6" href="chat" target="_blank">
							<div>
								<div class="ct-box col-xl-12 box box-chat">
									<h3 class="box-title">Chat</h3>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>

			<!-- Div vidéo et annuaires-->
			<div class="ct-merged-nt-an col-xl-12 col-lg-12">
				<?php 
					$video_enabled		=		get_field("field_5d4bce1e655a7");
					$video_link			=		get_field("field_5d4bcd74655a5");
					$video_title		=		get_field("field_5d4bd12a758e1");
					if ($video_enabled == true) { 
				?>

				<!-- Div vidéo -->
				<div class="ct-notes ct-container ct-container-xs col-xl-12">
					<h2 class="ct-title"><?php echo $video_title ?></h2>
					<div class="embedresize">
						<div>
							<iframe allowfullscreen frameborder="0" height="315" src="https://www.youtube.com/embed/<?php echo $video_link ?>" width="560"></iframe>
						</div>
					</div>
				</div>
				<?php } ?>

				<!-- Div accès rapide -->
				<div class="ct-agenda ct-container ct-container-xs col-xl-12">
					<h2 class="ct-title">Agenda</h2>
					<?php echo do_shortcode( '[events-calendar-templates template="default" style="style-3" category="all" date_format="default" start_date="" end_date="" limit="5" order="ASC" hide-venue="no" time="future" featured-only="false" columns="2" autoplay="true"]' ); ?>
				</div>
			</div>
		</div>

		<!-- Div colonne droite -->
		<div class="col-xl-6 col-lg-12">

			<!-- Div agenda -->
			<div class="col-xl-12 col-lg-12">
				<div class="ct-directory ct-container ct-container-xs col-xl-12">
					<h2 class="ct-title">Annuaire des établissements</h2>
					<?php echo do_shortcode('[searchform]'); ?>
				</div>
			</div>

			<!-- Div actualités internes -->
			<div class="col-xl-12 col-lg-12">
				<div class="ct-lastactu ct-container ct-container-xs col-xl-12">
					<h2 class="ct-title">Dernières actualités internes</h2>
					<?php do_shortcode("[login_content]"); ?>
					<?php 
						$args = array(
						    'post_type' 		=> 		'post',
						    'posts_per_page' 	=> 		1,
			              	'tax_query' 		=> 		array(
				                array(
				                  	'taxonomy' 		=> 		'category',
				                  	'field'    		=> 		'slug',
				                  	'terms'    		=> 		'actualites',
				                ),
			              	),
						);

						$query = new WP_Query( $args );
						    if ( $query->have_posts() ) {
						        while ( $query->have_posts() ) {
						            $query 			->		the_post();
						            $content 		= 		get_the_content();						            
					?>
					<article class="col-xl-12 d-flex row lastactu-item" id="post-<?php the_ID(); ?>">
						<div class="col-xl-4 col-lg-6">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'thumbnail' ); ?></a>
						</div>
						<div class="col-xl-8 col-lg-6">
							<a href="<?php the_permalink(); ?>"><?php the_title('<h3>', '</h3>'); ?></a> 
 							<p><?php echo wp_trim_words( $content , '20' ); ?></p>
							<div class="btn"><a href="<?php the_permalink(); ?>">Lire</a></div>
						</div>
					</article>
					<?php
						    }
						}
						wp_reset_postdata();
					?>
				</div>
			</div>

			<!-- Div raccourcis (si rôles) -->
			<?php display_my_shortcuts(); ?>

			<!-- Div Info ACSEA -->
			<div class="col-xl-12 col-lg-12">
				<div class="ct-news ct-container ct-container-xs col-xl-12">
					<h2 class="ct-title">Info ACSEA</h2>
					<div class="row justify-content-around">
						<?php 
						    $args = array(
						        'post_type' 		=> 		'info_acsea',
						        'posts_per_page' 	=>		3,
						    );
						    $i 					=		0;
						    $query 				= 		new WP_Query( $args );
							$actual_date 		= 		date("m/d/y");
						    $actual_date 		= 		strtotime($actual_date);

						    if ( $query->have_posts() ) {
						        while ( $query->have_posts() ) {
						     
						            $query 				->		the_post();
						            $i++;
						            $newspaper_date 	= 		get_the_date("m/d/y");
						            $newspaper_date 	= 		strtotime($newspaper_date);
						            $date_difference 	= 		$actual_date - $newspaper_date;
						            $howmany_days		= 		$date_difference/86400;
						            if ($howmany_days <= 14 and $i == 1) {
						                $recent="recent";
						            } else {
						                $recent="";
						            }
						?>
						<article class="col-xl-6 col-lg-6 col-md-6 art-<?php echo $i ?> <?php echo $recent ?>" id="post-<?php the_ID(); ?>">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<span class="btn is_recent">Nouveau !</span> 
								<?php echo the_post_thumbnail( 'info-acsea' );   ?>
							</a>
						</article>
						<?php
						        }
						    }
						    wp_reset_postdata();
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php get_footer(); ?>