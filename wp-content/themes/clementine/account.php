<?php 
/* Template Name: Account */
get_header(); ?> 

<div class="row col-xl-12">
  <div class="col-xl-3">
    <div class="ct-container col-xl-12 ct-shortcuts">
      <h2 class="ct-title">Ma photo de profil</h2>
      <?php get_currentuserinfo(); ?> 
      <?php echo do_shortcode('[avatar_upload]'); ?>
    </div>    
  </div>
  <div class="col-xl-9">
    <div class="ct-container ct-gallery col-xl-12">
      <div class="ct-title">
        <h2><?php the_title(); ?></h2>
        <span><?php the_excerpt(); ?></span>
      </div>
      <br>
      <div class="row col-xl-12 justify-content-between">
      	<div id="primary" class="content-area">
      		<main id="main" class="site-main">
      		<?php
      		while ( have_posts() ) :
      			the_post();

      			get_template_part( 'template-parts/content', get_post_type() );

      		endwhile; // End of the loop.
      		?>
      		</main>
	     </div>  
      </div>
    </div>  
  </div>
</div>

<?php
get_sidebar();
get_footer();