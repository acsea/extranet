<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 */
get_header();
$titre          =     get_field( "titre" );
$ets            =     get_field( "etablissement_ou_service" );
$description    =     get_field( "description" );
$vimeo_link     =     get_field( "vimeo_link" );
$youtube_link   =     get_field( "youtube_link" );
$date           =     get_field("date");
?>

<div class="row col-xl-12">
    <div class="col-xl-9">
        <div class="ct-gallery ct-video col-xl-12">
            <div class="row col-xl-12 justify-content-between">
      	        <div id="primary" class="content-area">
		                <main id="main" class="site-main">
                        <?php if ($vimeo_link) {
                          //Parse l'URL donnée par l'utilisateur afin de ne conserver que l'ID
                          $vimeo_link   =   parse_url($vimeo_link);
                          $vimeo_link   =   $vimeo_link['path'];
                        ?>
                        <div class="video-container">
                            <iframe src="https://player.vimeo.com/video<?php echo $vimeo_link ?>" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        </div>
                        <?php
                        // Traite l'URL donnée par l'utilisateur et ne garde que l'ID de la vidéo
                        } elseif ($youtube_link){
                          $youtube_link   =   parse_url($youtube_link);
                          $youtube_link   =   $youtube_link['query'];
                          $youtube_link   =   substr($youtube_link,2);
                          $removestr      =   stristr($youtube_link, '&t=');
                          if ($removestr) {
                              //Retire le repère temporel si existant
                              $youtube_link     =     str_replace($removestr, '', $youtube_link);
                          }
                          //Détermine si l'URL donnée est une playlist
                          $is_playlist    =   stristr($youtube_link, 'list');
                        ?>

                        <div class="embedresize">
                            <div>
                                <?php if ($is_playlist) { ?>
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?<?php echo $youtube_link ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                <?php  
                                } else{ 
                                ?>
                                    <iframe allowfullscreen frameborder="0" height="315" src="https://www.youtube.com/embed/<?php echo $youtube_link ?>" width="560"></iframe>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        } ?>
                        <div class="ct-container">
                            <h2><?php the_title(); ?></h2>
                            <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?>
                            </div>
                            <h3>Date</h3>
                            Ajoutée le <?php echo get_the_date(); ?>.
                            <h3>Établissement</h3>
                            <?php echo $ets ?>
                            <h3>Description</h3>
                            <?php 
                                echo $description;
                                if ( comments_open() || get_comments_number() ) :
                                  comments_template();
                                endif;
                            ?>
                        </div>
		                </main>
	              </div><
            </div>
        </div>  
    </div>
    <div class="col-xl-3">
        <div class="ct-container ct-related col-xl-12">
            <h2 class="ct-title">Autres vidéos</h2>   
                <?php 
                    $args   = array(
                        'post_type'         =>    'videos',
                        'posts_per_page'    =>    10,
                        'orderby'           =>    'rand',
                        'order'             =>    'DESC',
                  );

                    $query  = new WP_Query( $args );
                    if ( $query->have_posts() ) {
                        while ( $query->have_posts() ) {
                            $query->the_post();
                ?>
                <article id="post-<?php the_ID(); ?>" class="col-lg-12">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <div class="ct-inner-img">
                            <?php echo the_post_thumbnail( 'medium' );   ?>
                        </div>
                    </a>
                    <div class="box-meta">
                        <h3>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <?php the_title(); ?>
                            </a> 
                        </h3>          
                    </div>
                </article>
                <?php
                      }
                  }
                  wp_reset_postdata();
                  get_footer(); 
                ?>
        </div>    
    </div>
</div>
<?php
get_sidebar();
get_footer();
