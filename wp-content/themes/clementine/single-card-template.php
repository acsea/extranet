<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 */
acf_form_head();
get_header();

?>
<div class="row col-xl-12 validation-single">
    <div class="col-xl-3">
        <div class="ct-container ct-more-info col-xl-12">
            <div class="ct-title">
                <h2>Validation</h2>
            </div>
            <div class="ct-check-card">
		            <div class="alert alert-warning" role="alert">
		            Si la carte suivante est correcte et que cette demande est motivée par un besoin particulier dans le cadre de la mission du professionnel, veuillez la valider en cliquant sur "Oui" puis en envoyant le formulaire. <strong>Cette validation fait office de signature de la direction de l'établissement</strong>.
		            </div>
            </div>
        </div>    
    </div>
    <div class="col-xl-9">
        <div class="ct-container ct-gallery col-xl-12">
            <div class="ct-title">
                <h2><?php the_title(); ?></h2>
                <?php include "breadcrumb.php"; ?>
            </div>
            <div class="row col-xl-12 justify-content-between">
      	        <div id="primary" class="content-area">
		                <main id="main" class="site-main listener-validation">
                        <?php acf_form(array(
                            'post_id'               =>      $post->ID,
                            'post_title'            =>      false,
                            'html_updated_message'  =>      '<div class="alert alert-success" role="alert"><p>Carte professionnelle validée avec succès !</p></div>',
                            'submit_value'          =>      'Valider',

                        )); ?>
		                </main>
                    <div class="btn padding10-20 is-grey waiting-for-validation">Merci de valider la demande pour la soumettre</div>
	              </div>  
            </div>
        </div>  
    </div>
</div>
<?php
get_sidebar();
get_footer();
