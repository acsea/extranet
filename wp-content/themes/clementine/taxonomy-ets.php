<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package clementine
 */
get_header();
$terms = get_terms( 'ets', $args ); 
$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
?>
<div class="row col-xl-12">
    <div class="col-xl-3">
        <div class="ct-container col-xl-12 ct-inner-sidebar eq-padding ">
            <a href="add" class="btn padding10-20 no-mt">Ajouter un album</a>
        </div>
        <div class="ct-container col-xl-12 ct-inner-sidebar">
            <h2 class="ct-title">Trier par établissement</h2>
            <?php 
                $args     =     array('hide_empty' => true);
                $terms    =     get_terms( 'ets', $args );

                foreach ( $terms as $term ) {

                    // The $term is an object, so we don't need to specify the $taxonomy.
                    $term_link = get_term_link( $term );
                    // If there was an error, continue to the next term.
                    if ( is_wp_error( $term_link ) ) { continue; }
                    // We successfully got a link. Print it out.
                    echo '<div><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a>';
                    echo  term_description($post->ID,$term).'</div>';
                  }
            ?>
        </div>    
    </div>
    <div class="col-xl-9">
        <div class="col-xl-12">
            <div class="ct-container ct-gallery col-xl-12">
                <div class="ct-title">
                    <h2>Médias de l'établissement <?php echo $current_term->name; ?></h2>
                    <?php include "breadcrumb.php"; ?> 
                </div>
                <br>
                <div class="row col-xl-12 justify-content-between">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="col-lg-6 col-xl-4">
                        <article id="post-<?php the_ID(); ?>" class="col-lg-12">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                <div class="ct-inner-img">
                                  <?php echo the_post_thumbnail( 'medium' );   ?>
                                </div>
                            </a>
                            <div class="box-meta">
                                <h3>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                        <?php the_title(); ?>
                                    </a> 
                                </h3>
                                <?php the_excerpt(); ?>           
                            </div>
                        </article>
                    </div>
                    <?php endwhile; else : get_template_part( 'template-parts/content', 'none' ); endif; ?>

    	          </div><!-- #primary -->
            </div>  
        </div>
    </div>
</div>
<?php
get_sidebar();
get_footer();
