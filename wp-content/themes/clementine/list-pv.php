<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package clementine
 * Template Name: PV - Liste
 
 */

get_header();

// $terms = get_terms( 'ets', $args );
$date = get_the_date();
$time = get_the_time();

?>

<div class="row col-xl-12">
    <div class="col-xl-3">
              <?php 
                    $roles=$current_user->roles;
                    if (in_array("cce", $roles) or (in_array("administrator", $roles))){  ?>
    <div class="ct-container col-xl-12 ct-inner-sidebar eq-padding">

                      <a href="ajouter-un-pv/" class="btn width-100 padding10-20">Ajouter un PV</a>                    

    </div>
              <?php 
                    }
              ?>
    <div class="ct-container col-xl-12 ct-inner-sidebar">
      <h2 class="ct-title">CCE</h2>
          <?php echo do_shortcode('[subpages depth="-1" childof="1248"]'); ?>
    </div>
  </div>
  <div class="col-xl-9">
    <div class="ct-container ct-docs col-xl-12">
      <div class="ct-title">
        <h2><?php the_title(); ?></h2>
        <div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
      </div>
      <div class="row col-xl-12 justify-content-between">
          <?php
          $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
          $args = array(

              'post_type' => 'pv',
              'posts_per_page' => 12

          );

          $query = new WP_Query( $args );
           if ( $query->have_posts() ) {
             
                while ( $query->have_posts() ) {
             
                    $query->the_post();
          $fichier = get_field ('field_5d14da81e30c6');
          $description = get_field ('field_5d14da7be30c5');

 

          ?>
          <article id="post-<?php the_ID(); ?>" class="col-xl-4 col-lg-6">
            <div class="box-meta">
              <div class="meta-txt">
              <h3 class="meta-title">
                  <?php the_title(); ?>
              </h3>
              <div class="meta-description"><?php echo $description ?></div>
                <div class="meta-date">
                  <span><?php the_date(); ?></span>
                </div>
              <a href="<?php echo $fichier ?>"><span class="meta-link btn padding10-20">Télécharger</span></a>

              <?php 
                    $roles=$current_user->roles;
                    if (in_array("cce", $roles) or (in_array("administrator", $roles))){  
                      echo delete_post(); 
                    }
              ?>

            </div>
            </div>
          </article>
      <?php
                }
             
            } else{ ?>
            <span class="alert alert-danger">Cette section est vide !</span>
              <?php
            }
              next_posts_link( 'Older Entries', $query->max_num_pages );
              previous_posts_link( 'Next Entries &raquo;' ); 
        wp_reset_postdata();
       
      ?>
  </div><!-- #primary -->
    </div>  
  </div> 

<?php
get_sidebar();
get_footer();
