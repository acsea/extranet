<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package clementine
 */

get_header();

?>

<div class="message">
	<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/404.png" alt=""><br>
	<p>La page que vous cherchez est introuvable. Vous allez être redirigé vers l'accueil dans 5 secondes ...</p>
</div>


<script>
//Using setTimeout to execute a function after 5 seconds.
setTimeout(function () {
   //Redirect with JavaScript
   window.location.href= 'http://localhost:8888/extranet/';
}, 5000);
</script>

<?php
get_footer();
