<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package clementine
 */
get_header("ia");
?>
<div class="ct-container ct-directory col-xl-12">
    <div id="primary" class="content-area">
        <main id="main" class="site-main">
            <?php
                while ( have_posts() ) :
                    the_post();
                    get_template_part( 'template-parts/content-actu', get_post_type() ); 
            ?>
            <span class="btn">
                <a href="../../actualites/info-acsea/">Retour à la liste des numéros</a>
            </span>
            <?php
            endwhile;
            ?>
        </main>
    </div>
</div>
<?php
get_sidebar();
get_footer();
