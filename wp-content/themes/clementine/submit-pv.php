<?php /*Template Name: Formulaire ajout PV*/;?>
<?php acf_form_head(); ?>
<?php get_header(); ?>

<div id="container" class="col-xl-12 ask-form" >
	<div class="ct-container">
	    <div class="row">
	    	<div class="col-sm-12">

			<?php /* The loop */ ?>
						<?php /* The loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>
				
				<div class="ct-title">
				<h2><?php the_title(); ?></h2>
   	     			<div class="breadcrumb"><?php bcn_display($return = false, $linked = true, $reverse = false, $force = false); ?></div> 
				</div>
				<?php the_content(); ?>
				<!------------>
		
				<?php  $options = array(
					'post_id'				=> 'new_pv',
					'field_groups' 			=> array(3102),
					'post_title'			=> false,
					'post_type'				=> 'pv',
					'post_status'			=> 'publish',
			        'return' => '?my_param=true&updated=true',
					'submit_value'			=> 'Publier',
					'html_updated_message'	=> '<div class="message-ok alert alert-success" class="updated"><p>PV publiée avec succès !</p></div>',
				);
				 acf_form($options);
				 ?>

			<?php endwhile; ?>
			</div>
		</div><!-- #content -->
	</div><!-- #primary -->
</div>

<?php get_footer(); ?>