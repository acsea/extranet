<?php 
/* Template Name: Mouvements du personnel */
get_header();
$arrivees             =     get_field("arrivees");
$departs              =     get_field("departs");
$mobilitees           =     get_field("mobilitees");
$departs_en_retraite  =     get_field( "departs_en_retraite");
$title                =     get_the_title();
?>
<div class="row col-xl-12 ct-mvmts">
    <div class="col-xl-2">
        <div class="ct-container ct-archives col-xl-12">
            <h2 class="ct-title">Archives</h2>
            <ul>
                <?php 
                    $args   = array(
                        'post_type'       =>      'mouvements',
                        'posts_per_page'  =>      5,
                    );
                    $query  = new WP_Query( $args );
                    if ( $query->have_posts() ) {
                        while ( $query->have_posts() ) {
                            $query->the_post();
                ?>
                <li><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        <?php the_title(); ?></a></li>
                <?php
                    }   
                }
                wp_reset_postdata();
                ?>
 	          </ul>
        </div>
      </div>
      <div class="col-xl-5">
          <div class="ct-container col-xl-12">
              <h2 class="ct-title">Arrivées</h2>
                  <?php echo $arrivees; ?>
          </div>
      </div>
      <div class="col-xl-5">
          <div class="ct-container col-xl-12">
              <h2 class="ct-title">Départs</h2>
              <?php echo $departs; ?>
          </div>
      </div>
      <div class="col-xl-5 offset-xl-2">
          <div class="ct-container col-xl-12">
              <h2 class="ct-title">Mobilités</h2>
              <?php echo $mobilitees; ?>
          </div>  
      </div>
      <div class="col-xl-5">
          <div class="ct-container col-xl-12">
              <h2 class="ct-title">Départs en retraite</h2>
              <?php echo $departs_en_retraite; ?>
          </div>  
      </div>
</div>
<?php
get_footer();