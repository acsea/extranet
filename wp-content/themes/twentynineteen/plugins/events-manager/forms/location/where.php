<?php
#===============================================
# This file is part of the Events Manager - OpenStreetMaps plugin by Stonehenge Creations.
# https://wordpress.org/plugins/stonehenge-em-osm/
# It is used by Events Manager to show the front-end submission form for locations.
# DO NOT DELETE!
# Copy to: your-theme-folder/plugins/events-manager/forms/location/where.php
# VERSION: 1.8.3
#===============================================

global $EM_Event, $EM_Location;
return Stonehenge_EM_OSM::show_osm_meta_box();
